package com.tatahealth.ConsumerAppointment.Billing;

import com.tatahealth.API.Billing.Login;
import com.tatahealth.API.Billing.PaymentDetails;
import com.tatahealth.API.libraries.ExcelIntegration;

public class GenerateBillCases extends Login {
	public void appointmentConsumer(String paymentType) throws Exception {
		 readData();
		 consumerLogin();
		 PaymentModeCases pmop = new PaymentModeCases();
		 pmop.bookAppointment(paymentType); ExcelIntegration excelIn = new
		 ExcelIntegration(); excelIn.deleteFile(); excelIn.createSheet();
		 excelIn.appointment_details_header("Appointment Deatils");
		 excelIn.appointment_services(paymentObjectMapping); consumerLogout();
	}
}
