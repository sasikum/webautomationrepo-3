package com.tatahealth.EMR.pages.PatientAdvanceSearch;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class AdvancePatientSearch {
	
	public WebElement getEmrMenu(WebDriver driver,ExtentTest logger) 
	{
		WebElement emrMenu = Web_GeneralFunctions.findElementbyXPath("//a[@id='menu-toggle']//img", driver, logger);
 		return emrMenu;	
	}
	
	public static WebElement getAdvanceSearchBtn(WebDriver driver,ExtentTest logger) 
	{
		WebElement advanceSearchBtn = Web_GeneralFunctions.findElementbySelector("#advanceSearch",driver,logger);
		return advanceSearchBtn;	
	}
	
	public WebElement getAdvanceSearchLabel(WebDriver driver,ExtentTest logger)
	{
		WebElement advanceSearchlbl= Web_GeneralFunctions.findElementbyXPath("//h2[contains(text(),'Advanced Search')]",driver,logger);
		return advanceSearchlbl;	
	}
	
	public WebElement getSearchBtn(WebDriver driver,ExtentTest logger) {
		WebElement searchBtn= Web_GeneralFunctions.findElementbySelector("#advancedSearchButton",driver,logger);
		return searchBtn;	
	}

	
	public WebElement getModuleFromLeftMenu(WebDriver driver,ExtentTest logger,String moduleName)
	{
	
		WebElement element= Web_GeneralFunctions.findElementbyXPath("//*[@id='sidebarlinks']//a[text()='"+moduleName+"']", driver, logger);
 		return element;	
	}
	
	public WebElement getSearchLabels(WebDriver driver,ExtentTest logger,String searchText) 
	{
		WebElement element= Web_GeneralFunctions.findElementbyXPath("//div[@class='form-group']/label[contains(.,'"+searchText+"')]", driver, logger);
 		return element;	
	}
	
	public WebElement getSearchNameText(WebDriver driver,ExtentTest logger) {
		WebElement searchName= Web_GeneralFunctions.findElementbySelector("input#as_name",driver,logger);
		return searchName;	
	} 
	
	public WebElement getSearchAgeText(WebDriver driver,ExtentTest logger) {
		WebElement searchName= Web_GeneralFunctions.findElementbySelector("input#as_age",driver,logger);
		return searchName;	
	} 
	
	public List<WebElement> getPatientNameSearchResultSelect(WebDriver driver,ExtentTest logger) 
	{
		List<WebElement> elementList= Web_GeneralFunctions.listOfElementsbyXpath("//*[@class='pull-left']//*[@class='appointmentdoctorname']", driver, logger);
 		return elementList;			
	}
	
	public List<WebElement> getPatientAgeSearchResultSelect(WebDriver driver,ExtentTest logger) 
	{
		List<WebElement> elementList= Web_GeneralFunctions.listOfElementsbyXpath("//*[@class='pull-left']//*[@class='graytext']", driver, logger);
 		return elementList;	
 		
	}
	public WebElement getHeader(WebDriver driver,ExtentTest logger,String moduleName)
	{
		WebElement searchName= Web_GeneralFunctions.findElementbyXPath("//div[@class='innerpagehearderbg' and text()='"+moduleName+"']",driver,logger);
		return searchName;
	}
	
	public WebElement getAdvanceSearchNoResultFoundText(WebDriver driver,ExtentTest logger) 
	{
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//center[contains(text(),'No results found')]",driver, logger);
 		return element;		
	}
	
	public WebElement getDOBTextField(WebDriver driver,ExtentTest logger) 
	{
		WebElement dob= Web_GeneralFunctions.findElementbySelector("input#as_dob",driver,logger);
		return dob;	
	}
	
	public WebElement getDOBDatePicker(WebDriver driver,ExtentTest logger)
	{
		WebElement dtpicker=Web_GeneralFunctions.findElementbySelector("div[class*='datetimepicker']", driver, logger);
		return dtpicker;
	}
	
	public WebElement getCurrentDate(WebDriver driver,ExtentTest logger)
	{
		WebElement currdate=Web_GeneralFunctions.findElementbySelector("div[class*='datepicker-days'] td[class*='today']", driver, logger);
		return currdate;
	}
	
	public synchronized void selectPastDate(String DateofBirth,WebDriver driver,ExtentTest logger) {
		String[]s=DateofBirth.split(" ");
		String day=s[0],month=s[1],year=s[2];
		String xpath="//th[contains(@title,'Select Month')]";
		WebElement selectmonthbtn=Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		Web_GeneralFunctions.click(selectmonthbtn, "Clicking on Select month", driver, logger);

		xpath="//span[contains(@title,'Previous Year')]";
		WebElement prevyearbtn=Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		xpath="//th[contains(@title,'Select Year')]";
		
		WebElement selectyearbtn=Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		while(true)
		{
			
			if(selectyearbtn.getText().equals(year))
				break;
			else
				Web_GeneralFunctions.click(prevyearbtn,"Selecting previous years",driver,logger);
		}
	
		xpath="//div[@class='datepicker-months']//span[text()='"+month+"']";
		WebElement monthname=Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		Web_GeneralFunctions.click(monthname, "Selecting birth month", driver, logger);

		xpath="//div[@class='datepicker-days']//td[@class='day' and text()='"+day+"']";
		WebElement date=Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		Web_GeneralFunctions.click(date, "Selecting birth ate", driver, logger);
	}
	
	public WebElement getFutureDate(WebDriver driver,ExtentTest logger)
	{
		int tomorrow=Integer.parseInt(getCurrentDate(driver, logger).getText())+1;
		String xpath="//div[@class='datepicker-days']//td[text()='"+String.valueOf(tomorrow)+"']";
		WebElement futuredate=Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return futuredate;
	}
	
	public WebElement getToastMessage(WebDriver driver,ExtentTest logger)
	{
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message']",driver, logger);
 		return element;
	}
	
	public WebElement getViewDetailsBtn(WebDriver driver,ExtentTest logger)
	{
		WebElement element=Web_GeneralFunctions.findElementbyXPath("//li[1]//div[3]//a[1]", driver, logger);
		return element;
	}
	public WebElement getPatientDOB(WebDriver driver,ExtentTest logger)
	{
		WebElement element=Web_GeneralFunctions.findElementbyXPath("//input[@name='patientDOB']", driver, logger);
		return element;
	}
	public WebElement getGender(WebDriver driver,ExtentTest logger)
	{
		WebElement element=Web_GeneralFunctions.findElementbySelector("select#as_gender", driver, logger);
		return element;
	}
	public List<WebElement> getGenderDropdown(WebDriver driver,ExtentTest logger)
	{
		WebElement element=Web_GeneralFunctions.findElementbyXPath("//select[@name='as_gender']", driver, logger);
		Select s=new Select(element);
		return s.getOptions();
	}
	public void getPatientGenderDropdown(WebDriver driver,ExtentTest logger,String value) throws Exception 
	{
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//Select[@id='as_gender']", driver, logger);
		Web_GeneralFunctions.selectElementByVisibleText(element, value,"select patient gender", driver, logger);
 		
	}
	
	public WebElement getSelectedGenderOption(WebDriver driver,ExtentTest logger) {
		WebElement element=Web_GeneralFunctions.findElementbyXPath("//select[@name='as_gender']", driver, logger);
		Select s=new Select(element);
		return s.getFirstSelectedOption();
	}
	
	public WebElement getUHIDTextField(WebDriver driver,ExtentTest logger) 
	{
		WebElement uhid= Web_GeneralFunctions.findElementbySelector("input#as_uhid",driver,logger);
		return uhid;	
	}
	public WebElement getUHIDDetail(WebDriver driver,ExtentTest logger)
	{		
		WebElement uhid= Web_GeneralFunctions.findElementbySelector("div[class*='padleftright']>div[class='appointmentdoctorname']",driver,logger);
		return uhid;
	}
	
	public WebElement getMobileField(WebDriver driver,ExtentTest logger) 
	{
		WebElement mobile= Web_GeneralFunctions.findElementbySelector("input#as_mobile",driver,logger);
		return mobile;	
	}
	
	public List<WebElement> getMobileNoFromSearchResult(int num,WebDriver driver,ExtentTest logger)
	{		
		List<WebElement> mobileno= Web_GeneralFunctions.listOfElementsbyXpath("//ul[@id='advancedSearchresult']/li["+num+"]//div[@class='graytext btnpadtop']",driver,logger);
		return mobileno;
	}
	
	public List<WebElement> getPatientNameSearchResult(int num,WebDriver driver,ExtentTest logger) 
	{
		List<WebElement> elementList= Web_GeneralFunctions.listOfElementsbyXpath("//ul[@id='advancedSearchresult']/li["+num+"]//div[@class='appointmentdoctorname']", driver, logger);
 		return elementList;			
	}
	public List<WebElement> getMobileNoFieldFromSearchResult(WebDriver driver,ExtentTest logger)
	{		
		List<WebElement> mobileno= Web_GeneralFunctions.listOfElementsbyXpath("//div[@class='graytext btnpadtop']",driver,logger);
		return mobileno;
	}
	
	public List<WebElement> getUHIDFromSearchResult(WebDriver driver,ExtentTest logger)
	{		
		List<WebElement> mobileno= Web_GeneralFunctions.listOfElementsbyXpath("//div[contains(@class,'padleftright')]//div[@class='appointmentdoctorname']",driver,logger);
		return mobileno;
	}
	
	public List<WebElement> getResultContainer(WebDriver driver,ExtentTest logger)
	{		
		List<WebElement> container= Web_GeneralFunctions.listOfElementsbyXpath("//ul[@id='advancedSearchresult']/li",driver,logger);
		return container;
	}
	
	
	public List<WebElement> getProfilePicFromSearchResult(WebDriver driver,ExtentTest logger)
	{		
		List<WebElement> mobileno= Web_GeneralFunctions.listOfElementsbyXpath("//div[@class='profile-appointment']/img",driver,logger);
		return mobileno;
	}
	public WebElement getClinicID(WebDriver driver,ExtentTest logger) 
	{
		WebElement mobile= Web_GeneralFunctions.findElementbySelector("input#as_clinicId",driver,logger);
		return mobile;	
	}
	public WebElement getClinicIDInPatientDetails(WebDriver driver,ExtentTest logger)
	{
		WebElement mobile= Web_GeneralFunctions.findElementbySelector("input#clinic_id",driver,logger);
		return mobile;	
	}
	
	public WebElement getAddressText(WebDriver driver,ExtentTest logger) 
	{
		WebElement address= Web_GeneralFunctions.findElementbySelector("input#as_address",driver,logger);
		return address;	
	}
	
	public WebElement getAddressLineFromViewDetails(WebDriver driver,ExtentTest logger) 
	{
		WebElement address= Web_GeneralFunctions.findElementbySelector("input#addressLine1",driver,logger);
		return address;	
	}
	
	public WebElement getMoreOptions(WebDriver driver,ExtentTest logger) 
	{
		WebElement address= Web_GeneralFunctions.findElementbySelector("a#openMoreField",driver,logger);
		return address;	
	}
	public WebElement getStartDat(WebDriver driver,ExtentTest logger)
	{
		WebElement strtdt=Web_GeneralFunctions.findElementbySelector("input#as_startDate", driver, logger);
		return strtdt;
	}
	
	public WebElement getEndDat(WebDriver driver,ExtentTest logger)
	{
		WebElement enddt=Web_GeneralFunctions.findElementbySelector("input#as_endDate", driver, logger);
		return enddt;
	}

	public WebElement getCalendarPopup(WebDriver driver,ExtentTest logger)
	{
		WebElement calndr=Web_GeneralFunctions.findElementbySelector("div[class*='datetimepicker']", driver, logger);
		return calndr;
	}
	
	public WebElement getVisitFromToastMessage(WebDriver driver,ExtentTest logger)
	{
		WebElement message=Web_GeneralFunctions.findElementbySelector("div[class*='popover-danger']>div[class*='popover-content']", driver, logger);
		return message;		
	}
	
	public synchronized void selectDate(String strtdate,WebDriver driver,ExtentTest logger) {
		
		String[]s=strtdate.split(" ");
		String day=s[0],month=s[1],year=s[2];
		String xpath="//th[contains(@title,'Select Month')]";
		WebElement selectmonthbtn=Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		Web_GeneralFunctions.click(selectmonthbtn, "Clicking on Select month", driver, logger);

		xpath="//span[contains(@title,'Previous Year')]";
		WebElement prevyearbtn=Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		
		xpath="//span[contains(@title,'Next Year')]";
		WebElement nextyearbtn=Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		
		xpath="//th[contains(@title,'Select Year')]";
		WebElement selectyearbtn=Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		while(true)
		{
			
			if(selectyearbtn.getText().equals(year))
				break;
			else if(Integer.parseInt(year)<Integer.parseInt(selectyearbtn.getText()))
				Web_GeneralFunctions.click(prevyearbtn,"Selecting previous year",driver,logger);
			else 
				Web_GeneralFunctions.click(nextyearbtn,"Selecting next year",driver,logger);
		}
	
		xpath="//div[@class='datepicker-months']//span[text()='"+month+"']";
		WebElement monthname=Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		Web_GeneralFunctions.click(monthname, "Selecting birth month", driver, logger);

		xpath="//div[@class='datepicker-days']//td[(@class='day' or @class='day weekend') and text()='"+day+"']";
		WebElement date=Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		Web_GeneralFunctions.click(date, "Selecting birth ate", driver, logger);
	}
	
	public WebElement getGovtID(WebDriver driver,ExtentTest logger)
	{
			WebElement govtID=Web_GeneralFunctions.findElementbySelector("input#as_govtId", driver, logger);
			return govtID;
	}
	
	public WebElement getGovtIDFromPatientDetails(WebDriver driver,ExtentTest logger)
	{
			WebElement govtID=Web_GeneralFunctions.findElementbySelector("input#government_id_number", driver, logger);
			return govtID;
	}
	
	public WebElement getSaveBtn(WebDriver driver,ExtentTest logger)
	{
			WebElement saveBtn=Web_GeneralFunctions.findElementbyXPath("//a[@id='btnMedical' and text()=' Save ']", driver, logger);
			return saveBtn;
	}	
	
	public WebElement getBookAppt_ConsulationBtn(WebDriver driver,ExtentTest logger,String text) {
		
		WebElement btn=Web_GeneralFunctions.findElementbyXPath("//div/a[@class='cursorPointer' and text()='"+text+"']",driver,logger);
		return btn;
	}
	
	public WebElement getReasonForDocVisit(WebDriver driver,ExtentTest logger) {
		WebElement reason=Web_GeneralFunctions.findElementbySelector("textarea#reasonDoctorVisitRegister", driver, logger);
		return reason;
	}
	
	
	public WebElement getSaveAndConfirm(WebDriver driver,ExtentTest logger) {
		WebElement saveConfirm=Web_GeneralFunctions.findElementbySelector("input#submitOnce", driver, logger);
		return saveConfirm;
	}
	
	public WebElement getAppointmentPhoto(WebDriver driver,ExtentTest logger, String patientName)
	{
		String xpath="//div[contains(text(),'"+patientName+"')]/parent::div/preceding-sibling::div/div";
		WebElement saveConfirm=Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return saveConfirm;
	}
	
	
	public WebElement getCancelBtnOnPatientDetails(WebDriver driver,ExtentTest logger)
	{
		WebElement cancelBtn=Web_GeneralFunctions.findElementbySelector("a#patientEditCancelBtn", driver, logger);
		return cancelBtn;
	}
	
	public WebElement getAppointmentDropdown(WebDriver driver,ExtentTest logger, String patientName) {
		String xpath="//div[contains(text(),'"+patientName+"')]/../../following-sibling::div[contains(@class,'padtop')]//a[@class='dropdown-toggle']";
		WebElement saveConfirm=Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return saveConfirm;
	}
	
	public String getCurrSysDate() {
		LocalDate date = LocalDate.now();
		DateTimeFormatter formatters = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		String text = date.format(formatters);
		return text;
	}

	public boolean isValueEntered(WebElement element,String name){
		String txtValue = element.getAttribute("value");
		return (txtValue.isEmpty()) ? false : txtValue.equalsIgnoreCase(name);
	}
	
	public String getFormattedDate(String enteredDate) throws ParseException
	{ 
		SimpleDateFormat sdf=new SimpleDateFormat("dd MMM yyyy");
		Date date=sdf.parse(enteredDate);
		DateFormat df=new SimpleDateFormat("dd-MM-yyyy");
		return df.format(date);	
	}
	public List<WebElement> getInlineResultContainer(WebDriver driver,ExtentTest logger)
	{		
		List<WebElement> container= Web_GeneralFunctions.listOfElementsbyXpath("//ul[@id='patientsearchresult']/li",driver,logger);
		return container;
	}
	
	public WebElement getSignedUpUser(int num,WebDriver driver,ExtentTest logger)
	{
		WebElement cancelBtn=Web_GeneralFunctions.findElementbyXPath("//ul[@id='advancedSearchresult']/li["+num+"]//div[@class='graytext btnpadtop'][1]", driver, logger);
		return cancelBtn;
	}
}
