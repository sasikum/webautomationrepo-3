package com.tatahealth.EMR.pages.Consultation;

import java.util.List;
import java.util.Random;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class ReferralPage {

	
	public WebElement moveToReferral(WebDriver driver,ExtentTest logger) throws Exception{
		
		String xpath = "//ul[@id='fixedmenu']/li[@data-input='Refer']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement selectSpeciality(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//span[contains(text(),'Select Specialty')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement searchSpeciality(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@class='btn-group bootstrap-select show-tick open']//input[@class='form-control']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement selectOneSpeciality(int size,WebDriver driver,ExtentTest logger)throws Exception{
		
		/*String xpath = "//div[@class='btn-group bootstrap-select show-tick open']//div[@class='dropdown-menu open']/ul/li";
		List<WebElement> elements = Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);*/
		Random r = new Random();
		Integer speciality = r.nextInt(size);
		if(speciality == 0) {
			speciality+=1;
		}
		String xpath = "//div[@class='btn-group bootstrap-select show-tick open']//div[@class='dropdown-menu open']/ul/li["+speciality+"]/a/span[1]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement searchAndSelectSpeciality(String value,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//span[contains(text(),'"+value+"')]";
		System.out.println("search and select : "+xpath);
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement multispecialityFromArrayList(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@class='btn-group bootstrap-select show-tick open']//div[@class='dropdown-menu open']/ul/li";
		List<WebElement> elements = Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
		int size = elements.size();
		System.out.println("Size "+size);
		
		WebElement element = selectOneSpeciality(size, driver, logger);
		return element;
		
	}
	
	public WebElement getSpecialityButton(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@class='btn-group bootstrap-select show-tick open']/button";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getReferTo(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//label[contains(text(),'Refer To')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getMessage(WebDriver driver,ExtentTest logger)throws Exception{
	
		String xpath = "//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getReferToSelectElement(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//button[@class='btn dropdown-toggle bs-placeholder btn-default']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement sendReferDoctor(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@class='btn-group bootstrap-select open']//input[@class='form-control']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getSelectDoctor(String value,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//span[contains(text(),'"+value+"')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement saveReferral(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//ul[@id='fixedmenu']/li[@data-input='Print']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getComment(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//textarea[@id='referralComment']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getNoMatch(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//li[@class='no-results']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
		
	}
	
	public WebElement getSpecialityDropDownElement(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@class='btn-group bootstrap-select show-tick']/button";
		
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getSelectClinic(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//label[contains(text(),'Select Clinic')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getDeselectSpeciality(String value,WebDriver driver,ExtentTest logger) throws Exception{	
		
		String xpath = "//div[@class='btn-group bootstrap-select show-tick open']//input[@class='form-control']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);

		//Web_GeneralFunctions.sendkeys(element, value, "Send deselecting speciality", driver, logger);

		Web_GeneralFunctions generalFunctions = new Web_GeneralFunctions();
		generalFunctions.sendkeys(element, value, "Send deselecting speciality", driver, logger);

			
		xpath = "//div[@class='btn-group bootstrap-select show-tick open']//div[@class='dropdown-menu open']/ul/li[@class='selected active']/a/span[1]";
			
		element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
		
		
	}
	
	public WebElement getDoctorNoResult(WebDriver driver,ExtentTest logger)throws Exception{

		String xpath = "//div[@class='btn-group bootstrap-select open']/div/ul/li[@class='no-results']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
		
	}
	
	
	public WebElement getReferralElement(WebDriver driver,ExtentTest logger)throws Exception{

		String xpath = "//h4[contains(text(),'Referral')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
		
	}
	
	
}
