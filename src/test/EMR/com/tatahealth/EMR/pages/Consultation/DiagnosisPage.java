package com.tatahealth.EMR.pages.Consultation;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class DiagnosisPage {

	
	public WebElement moveToDiagnosisModule(WebDriver driver,ExtentTest logger) throws Exception{
		
		String xpath = "//ul[@id='fixedmenu']/li[@data-input='Prescription']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	

	public WebElement selectDiagnosisFromDropDown(WebDriver driver,Integer row,String value,ExtentTest logger) throws Exception{
	
		//String availableRow = getAvailableRow(driver, logger);
		//Web_GeneralFunctions generalFunctions = new Web_GeneralFunctions();
		String symptomField = "//input[@id='DiagnosisName"+row+"']";
		
		WebElement element = Web_GeneralFunctions.findElementbyXPath(symptomField, driver, logger);

		Web_GeneralFunctions.click(element, "Click diagnosis text field", driver, logger);
		
		Web_GeneralFunctions.sendkeys(element, value, "Sending search text to diagnosis field", driver, logger);
		Thread.sleep(1000);
		
		
		String xpath = "//ul[@class='ui-menu ui-widget ui-widget-content ui-autocomplete ui-front'][starts-with(@style,'display: block;')]/li";
		
		//System.out.println("xpath : "+xpath);
		
		element = Web_GeneralFunctions.selectDropDown(xpath,"", driver, logger,"Select values");
				
		return element;
	}
	
	public WebElement getCommonDiagnosis(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//h4[contains(text(),'Common Diagnoses')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement selectCommonDiagnosis(WebDriver driver,ExtentTest logger)throws Exception{

		Random r = new Random();
		
		int index = r.nextInt(3);
		if(index == 0) {
			index +=1;
		}
		String xpath = "//div[@id='RecentDiagnoses']/div/div[1]/label["+index+"]/a";
		
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getAddDiagnosisButton(WebDriver driver,ExtentTest logger)throws Exception{
		
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//a[@id='add_diagnosis_row']", driver, logger);
		return element;
	}
	
	public WebElement getDeleteDiagnosis(int row,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//tr[@id='diagnosisRow"+row+"']//button";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
		
	}
	
	public WebElement saveDiagnosis(WebDriver driver,ExtentTest logger)throws Exception{
		
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//ul[@id='fixedmenu']/li[@data-input='Print']", driver, logger);
		return element;
		
	}
	
	
	public WebElement getSuccessMessage(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message'][contains(text(),'Diagnosis details saved successfully')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getDiagnosisTextField(int row,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//input[@id='DiagnosisName"+row+"']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getJSErrorPopup(WebDriver driver,ExtentTest logger) throws Exception{
		
		//String xpath = "//div[@class='popover fade top in popover-danger'][contains(@style,'display: block;')]/div[2]";
		String xpath = "//div[@class='popover-content']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public List<WebElement> getNoMatchingDiagnosis(String value,int row,WebDriver driver,ExtentTest logger) throws Exception{
		
		String xpath = "//input[@id='DiagnosisName"+ row+"']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		Thread.sleep(1000);
		
		Web_GeneralFunctions generalFunctions = new Web_GeneralFunctions();
		generalFunctions.sendkeys(element, value, "Pass free text", driver, logger);

		Thread.sleep(1000);
		
		xpath = "//ul[@class='ui-menu ui-widget ui-widget-content ui-autocomplete ui-front'][starts-with(@style,'display: block;')]/li";
		List<WebElement> list = Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
		
		return list;
	}
	
	
	public WebElement getMessage(WebDriver driver,ExtentTest logger)throws Exception{
		
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message']", driver, logger);	
		return element;
	}
	
	public WebElement getDiagnosisByLinkText(String commonDiagnosisName,WebDriver driver,ExtentTest logger)throws Exception{
		
		WebElement element = Web_GeneralFunctions.findElementbyLinkText(commonDiagnosisName, driver, logger);
		return element;
	}
	
	public WebElement selectDuplicateDiagnosis(String commonDiagnosisName,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "";
		int i=1;
		WebElement element = null;
		while(i<=3) {
			xpath = "//div[@id='RecentDiagnoses']/div/div[1]/label["+i+"]/a";
			element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
			Web_GeneralFunctions.wait(1);
			if(element.getAttribute("title").equalsIgnoreCase(commonDiagnosisName)) {
				break;
			}
			else {
			i++;
			}
		}
		return element;
	}
	
	public WebElement selectStatus(int row,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//select[@id='status_"+row+"']";
		//System.out.println("xpath : "+xpath);
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;

	}
	
	public List<String> getDiagnosisData(int row,WebDriver driver,ExtentTest logger)throws Exception{
		List<String> listOfSavedDiagnosis = new ArrayList<String>();
		int i =0;
		String xpath = "";
		String value = "";
		int indexOf = 0;
		//System.out.println("row in diag : "+row);
		while(i<row) {
			xpath = "//input[@id='DiagnosisName"+i+"']";
			value = Web_GeneralFunctions.getAttribute(getDiagnosisTextField(i, driver, logger),"value","Get diagnosis attribute", driver, logger);
			indexOf = value.indexOf(" ");
			value = value.substring(indexOf+1);
			listOfSavedDiagnosis.add(value);
			//System.out.println("list");
			i++;
		}
		System.out.println("list added diagnosis : "+listOfSavedDiagnosis);
		return listOfSavedDiagnosis;
	}
	
	
	public WebElement selectStage(int row,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//select[@id='stage_"+row+"']";
		//System.out.println("xpath : "+xpath);
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;

	}
	
                    
	
}
