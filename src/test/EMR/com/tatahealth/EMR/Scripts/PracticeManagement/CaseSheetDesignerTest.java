
package com.tatahealth.EMR.Scripts.PracticeManagement;

import static org.testng.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.math.RandomUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.API.libraries.SheetsAPI;
import com.tatahealth.EMR.Scripts.Consultation.ConsultationTest;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.Consultation.DiagnosisPage;
import com.tatahealth.EMR.pages.Consultation.GlobalPrintPage;
import com.tatahealth.EMR.pages.Consultation.SymptomPage;
import com.tatahealth.EMR.pages.PracticeManagement.CaseSheetDesignerPages;

import com.tatahealth.EMR.pages.PracticeManagement.PracticeManagementPages;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;
import com.tatahealth.ReusableModules.Web_Testbase;

public class CaseSheetDesignerTest {
	
	public static String templateName = "";
	ExtentTest logger;
	DiagnosisPage dp = new DiagnosisPage();
	protected static List<String> caseSheet1 = new ArrayList<>();
	protected static List<String> caseSheet2 = new ArrayList<>();
	CaseSheetDesignerPages sheet = new CaseSheetDesignerPages();
	GlobalPrintPage gpp = new GlobalPrintPage();
	PracticeManagementTest pmt = new PracticeManagementTest();
	SymptomPage sp = new SymptomPage();
	
			@BeforeClass(alwaysRun=true,groups= {"Regression"})
			public static void beforeClass() throws Exception {
				Login_Doctor.executionName="EMR_PracticeManagement_CaseSheetDesigner";
				Login_Doctor.LoginTest();
				new MedicalKItTest().closePreviousConsultations();
				new PracticeManagementTest().moveToPracticeManagement();
			} 
			@AfterClass(groups = { "Regression"})
			public static void afterClass(){
				Login_Doctor.LogoutTest();
			}
			

	//Test Case PM_109 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=529)
	public synchronized void moveToCaseSheetDesignerAndValidate()throws Exception{
		
		logger = Reports.extent.createTest("EMR move to case sheet designer");
		Web_GeneralFunctions.click(sheet.getMoveToCaseSheetDesigner(Login_Doctor.driver, logger), "Move to case sheet designer", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getSelectCaseSheetDesignerDropDown(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getSelectButton(Login_Doctor.driver, logger));
		String dropdownText = Web_GeneralFunctions.getText(sheet.getSelectCaseSheetDesignerDropDown(Login_Doctor.driver, logger),"Get default text on dropdown",Login_Doctor.driver,logger);
		assertTrue(dropdownText.equals("None selected"));
		assertTrue(Web_GeneralFunctions.isDisplayed(sheet.getSelectButton(Login_Doctor.driver, logger)));
		assertTrue(sheet.getCaseSheetHeadingDisplayStatus(Login_Doctor.driver));
	}
	
	
	//Test Case PM_110 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=530)
	public synchronized void selectMultipleCaseSheetFromDropdown()throws Exception{
		
		logger = Reports.extent.createTest("EMR able to select multiple case sheet");
		PracticeManagementPages pmt = new PracticeManagementPages();
		Web_GeneralFunctions.click(pmt.getLeftMainMenu(Login_Doctor.driver, logger), "Click menu", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(sheet.getSelectCaseSheetDesignerDropDown(Login_Doctor.driver, logger), "click drop down button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getClinicalNotesElement(Login_Doctor.driver, logger));
		List<WebElement> allCaseSheetElements = sheet.getAllCaseSheetsFromDropDown(Login_Doctor.driver, logger);
		for(WebElement CaseSheet :allCaseSheetElements) {
			if(!Web_GeneralFunctions.isChecked(CaseSheet, Login_Doctor.driver)){
				Web_GeneralFunctions.click(CaseSheet, "Selecting every caseSheet checkbox", Login_Doctor.driver, logger);
			}
		}
		Web_GeneralFunctions.scrollUp(Login_Doctor.driver);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver,sheet.getSelectButton(Login_Doctor.driver, logger) );
		Web_GeneralFunctions.click(sheet.getSelectButton(Login_Doctor.driver, logger), "clicking on select button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, dp.getMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(dp.getMessage(Login_Doctor.driver, logger), "get error text", Login_Doctor.driver, logger);
		assertTrue(sheet.getActiveCaseSheetNameFromDropdown(Login_Doctor.driver, logger).contains("All selected"));
		assertTrue(text.equalsIgnoreCase("Duplicate sections removed"));
	}
	
	//Test Case PM_111 covered here
		@Test(groups= {"Regression","PracticeManagement"},priority=531)
		public synchronized void selectCaseSheetAndClickOnSelectButton()throws Exception{
			
			logger = Reports.extent.createTest("EMR select case sheet and verify");
			pmt.clickOrgHeader();
			pmt.moveToPracticeManagement();
			Web_GeneralFunctions.click(sheet.getMoveToCaseSheetDesigner(Login_Doctor.driver, logger), "Move to case sheet designer", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getSelectCaseSheetDesignerDropDown(Login_Doctor.driver, logger));
			Web_GeneralFunctions.click(sheet.getSelectCaseSheetDesignerDropDown(Login_Doctor.driver, logger), "click drop down button", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getClinicalNotesElement(Login_Doctor.driver, logger));
			Web_GeneralFunctions.click(sheet.getClinicalNotesElement(Login_Doctor.driver, logger),"selecting clinical Notes caseSheet", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(sheet.getSelectButton(Login_Doctor.driver, logger), "Clicking on select button", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getSaveCaseSheetButton(Login_Doctor.driver, logger));
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getPreviewCaseSheetButton(Login_Doctor.driver, logger));
			templateName = Web_GeneralFunctions.getText(sheet.getSelectCaseSheetDesignerDropDown(Login_Doctor.driver, logger),"get currently selected template Name", Login_Doctor.driver, logger);
			String ExaminationName = Web_GeneralFunctions.getText(Web_GeneralFunctions.findElementbyXPath(sheet.getCaseSheetExaminationsXpath(), Login_Doctor.driver, logger), "selecting Examinations Name in Selected CaseSheet", Login_Doctor.driver, logger);
			assertTrue(ExaminationName.split(":")[1].trim().equals(templateName));
		}
	
		//Test case PM_113 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=532)
	public synchronized void verifyPreviewCaseSheet()throws Exception{
		logger = Reports.extent.createTest("EMR click preview case sheet and verify");
		Web_GeneralFunctions.click(sheet.getSelectCaseSheetDesignerDropDown(Login_Doctor.driver, logger), "click on casesheet designer dropdown", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getHyperTensionElement(Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(sheet.getHyperTensionElement(Login_Doctor.driver, logger),"select hypertension sheet", Login_Doctor.driver, logger);
		Web_GeneralFunctions.scrollUp(Login_Doctor.driver);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver,sheet.getSelectButton(Login_Doctor.driver, logger) );
		Web_GeneralFunctions.click(sheet.getSelectButton(Login_Doctor.driver, logger), "Clicking on select button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getSaveCaseSheetButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getPreviewCaseSheetButton(Login_Doctor.driver, logger));
		caseSheet1 = sheet.getAllExaminationNamesAsList(Login_Doctor.driver,sheet.getCaseSheetExaminationsXpath() , logger);
		Web_GeneralFunctions.scrollToElement(sheet.getPreviewCaseSheetButton(Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.click(sheet.getPreviewCaseSheetButton(Login_Doctor.driver, logger), "get preview case sheet button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getPreviewCloseCaseSheetButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getPreviewCloseByXCaseSheetButton(Login_Doctor.driver, logger));
		caseSheet2  = sheet.getAllExaminationNamesAsList(Login_Doctor.driver,sheet.getPreviewCaseSheetExaminationsXpath() , logger);
		closePreviewCaseSheet();
		int randomExaminationIndex = RandomUtils.nextInt(caseSheet1.size());
		Web_GeneralFunctions.click(sheet.getAllExaminationCheckBoxesAsList(Login_Doctor.driver, logger).get(randomExaminationIndex), "Randomly unchecking an Examination checkbox", Login_Doctor.driver, logger);
		caseSheet1.remove(randomExaminationIndex);
		Web_GeneralFunctions.click(sheet.getPreviewCaseSheetButton(Login_Doctor.driver, logger), "get preview case sheet button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getPreviewCloseCaseSheetButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getPreviewCloseByXCaseSheetButton(Login_Doctor.driver, logger));
		caseSheet2  = sheet.getAllExaminationNamesAsList(Login_Doctor.driver,sheet.getPreviewCaseSheetExaminationsXpath() , logger);
		assertTrue(caseSheet1.equals(caseSheet2));
	}
	
	
	@Test(groups= {"Regression","PracticeManagement"},priority=533)
	public synchronized void closePreviewCaseSheet()throws Exception{
		logger = Reports.extent.createTest("EMR able to close preview case sheet");
		Web_GeneralFunctions.click(sheet.getPreviewCloseCaseSheetButton(Login_Doctor.driver, logger), "get close preview case sheet button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getSaveCaseSheetButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getPreviewCaseSheetButton(Login_Doctor.driver, logger));
	}
	
	@Test(groups= {"Regression","PracticeManagement"},priority=534)
	public synchronized void unselect1stSection()throws Exception{
		logger = Reports.extent.createTest("EMR unselect 1st section in case sheet");
		Web_GeneralFunctions.click(sheet.get1stSection(Login_Doctor.driver, logger), "unselect section", Login_Doctor.driver, logger);
	}
	
	@Test(groups= {"Regression","PracticeManagement"},priority=535)
	public synchronized void changesConfirmPopup()throws Exception{
		logger = Reports.extent.createTest("EMR verify confirm pop up display");
		Web_GeneralFunctions.scrollUp(Login_Doctor.driver);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getSelectButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(sheet.getSelectButton(Login_Doctor.driver, logger), "click select button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getSweetAlertPopUp(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(sheet.getSweetAlertPopUp(Login_Doctor.driver, logger), "get message from sweet alert pop up", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("Changes you made may not be saved. Do you want to continue?"));	
	}
	
	@Test(groups= {"Regression","PracticeManagement"},priority=536)
	public synchronized void clickNoPopup()throws Exception{
		logger = Reports.extent.createTest("EMR click no in sweet pop up and verify");
		Web_GeneralFunctions.click(sheet.getNoButtonInPopUp(Login_Doctor.driver, logger), "click no in pop up", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getSelectButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getPreviewCaseSheetButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getSaveCaseSheetButton(Login_Doctor.driver, logger));
	}
	
	@Test(groups= {"Regression","PracticeManagement"},priority=537)
	public synchronized void clickYesPopup()throws Exception{
		logger = Reports.extent.createTest("EMR yes in sweet pop up and verify");
		Web_GeneralFunctions.click(sheet.getSelectButton(Login_Doctor.driver, logger), "click select button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getSweetAlertPopUp(Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(sheet.getYesButtonInPopUp(Login_Doctor.driver, logger), "click yes in pop up", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getSelectButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getPreviewCaseSheetButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getSaveCaseSheetButton(Login_Doctor.driver, logger));
	}
	
	//PM_114 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=538)
	public synchronized void saveCaseSheetWithEmptyName()throws Exception{
		logger = Reports.extent.createTest("EMR save case sheet with empty name and verify toast message");
		clickSaveCaseSheetDesigner();
		Web_GeneralFunctions.click(sheet.getSaveCustomCaseSheetButton(Login_Doctor.driver, logger), "click save changes", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(sheet.getMessage(Login_Doctor.driver, logger), "get message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("Please enter template name"));
		Web_GeneralFunctions.wait(3);
	}
	
	//PM_114 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=539)
	public synchronized void saveCaseSheetWithValidName()throws Exception{
		logger = Reports.extent.createTest("EMR save case sheet with valid name and verify toast message");
		templateName = RandomStringUtils.randomAlphanumeric(7);
		String maxTemplateNamelength = Web_GeneralFunctions.getAttribute(sheet.getExaminationTemplateName(Login_Doctor.driver, logger), "maxlength", "get max allowable character limit", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(sheet.getExaminationTemplateName(Login_Doctor.driver, logger), templateName, "sending template name", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(sheet.getSaveCustomCaseSheetButton(Login_Doctor.driver, logger), "click save changes", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(sheet.getMessage(Login_Doctor.driver, logger), "get message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("Case sheet saved successfully"));
		assertTrue(Integer.parseInt(maxTemplateNamelength)==30);	
	}
	
	@Test(groups= {"Regression","PracticeManagement"},priority=540)
	public synchronized void saveCaseSheetWithoutSelectingAnyExaminationSection()throws Exception{
		logger = Reports.extent.createTest("EMR save case sheet without selecting any Examination section");
		pmt.clickOrgHeader();
		pmt.moveToPracticeManagement();
		Web_GeneralFunctions.click(sheet.getMoveToCaseSheetDesigner(Login_Doctor.driver, logger), "Move to case sheet designer", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getSelectCaseSheetDesignerDropDown(Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(sheet.getSelectCaseSheetDesignerDropDown(Login_Doctor.driver, logger), "click select case sheet button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getClinicalNotesElement(Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(sheet.getClinicalNotesElement(Login_Doctor.driver, logger), "click clinical notes", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(sheet.getSelectButton(Login_Doctor.driver, logger), "click select button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getPreviewCaseSheetButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getSaveCaseSheetButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(sheet.get1stSection(Login_Doctor.driver, logger), "unselect 1st section", Login_Doctor.driver, logger);
		clickSaveCaseSheetDesigner();
		Web_GeneralFunctions.sendkeys(sheet.getExaminationTemplateName(Login_Doctor.driver, logger), templateName, "sending template name", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(sheet.getSaveCustomCaseSheetButton(Login_Doctor.driver, logger), "save casesheet button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(sheet.getMessage(Login_Doctor.driver, logger), "get message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("Please select at least one section"));	
	}
	
	@Test(groups= {"Regression","PracticeManagement"},priority=541)
	public synchronized void saveCaseSheetWithExistingTemplateName()throws Exception{
		logger = Reports.extent.createTest("EMR save case sheet with existing template name");
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getPreviewCaseSheetButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getSaveCaseSheetButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(sheet.get1stSection(Login_Doctor.driver, logger), "unselect 1st section", Login_Doctor.driver, logger);
		clickSaveCaseSheetDesigner();
		Web_GeneralFunctions.click(sheet.getSaveCustomCaseSheetButton(Login_Doctor.driver, logger), "save casesheet button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(sheet.getMessage(Login_Doctor.driver, logger), "get message", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(sheet.getSavePopUpCloseButton(Login_Doctor.driver, logger), "close save case sheet", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getSelectButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.scrollUp(Login_Doctor.driver);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver,sheet.getSelectButton(Login_Doctor.driver, logger) );
		Web_GeneralFunctions.click(sheet.getMoveToCaseSheetDesigner(Login_Doctor.driver, logger),"click on caseSheet designer", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getSelectButton(Login_Doctor.driver, logger));
		assertTrue(text.equalsIgnoreCase("Template name already exists"));
	}
	
	//Test Case PM_112, PM_115 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=542)
	public synchronized void createCustomCaseSheetAndverify() throws Exception {
		logger = Reports.extent.createTest("EMR Verify saved CaseSheet in consultation page");
		pmt.clickOrgHeader();
		pmt.moveToPracticeManagement();
		Web_GeneralFunctions.click(sheet.getMoveToCaseSheetDesigner(Login_Doctor.driver, logger), "Move to case sheet designer", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getSelectCaseSheetDesignerDropDown(Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(sheet.getSelectCaseSheetDesignerDropDown(Login_Doctor.driver, logger), "click select case sheet button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getClinicalNotesElement(Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(sheet.getClinicalNotesElement(Login_Doctor.driver, logger), "selecing clinical notes ", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(sheet.getHyperTensionElement(Login_Doctor.driver, logger), "click clinical notes", Login_Doctor.driver, logger);
		Web_GeneralFunctions.scrollUp(Login_Doctor.driver);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver,sheet.getSelectButton(Login_Doctor.driver, logger) );
		Web_GeneralFunctions.click(sheet.getSelectButton(Login_Doctor.driver, logger), "click select button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getPreviewCaseSheetButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getSaveCaseSheetButton(Login_Doctor.driver, logger));
		caseSheet1 = sheet.getAllExaminationNamesAsList(Login_Doctor.driver, sheet.getCaseSheetExaminationsXpath(), logger);
		clickSaveCaseSheetDesigner();
		templateName = RandomStringUtils.randomAlphabetic(5);
		Web_GeneralFunctions.sendkeys(sheet.getExaminationTemplateName(Login_Doctor.driver, logger), templateName, "sending template name", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(sheet.getSaveCustomCaseSheetButton(Login_Doctor.driver, logger), "save casesheet button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getMessage(Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(sheet.getSelectCaseSheetDesignerDropDown(Login_Doctor.driver, logger), "click select case sheet button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getSavedCaseSheetName(Login_Doctor.driver, templateName, logger));
		Web_GeneralFunctions.click(sheet.getSavedCaseSheetName(Login_Doctor.driver, templateName, logger), "clicking on previously saved casesheet", Login_Doctor.driver, logger);
		Web_GeneralFunctions.scrollUp(Login_Doctor.driver);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver,sheet.getSelectButton(Login_Doctor.driver, logger) );
		Web_GeneralFunctions.click(sheet.getSelectButton(Login_Doctor.driver, logger), "click select button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getPreviewCaseSheetButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getSaveCaseSheetButton(Login_Doctor.driver, logger));
		caseSheet2 = sheet.getAllExaminationNamesAsList(Login_Doctor.driver, sheet.getCaseSheetExaminationsXpath(), logger);
		assertTrue(sheet.getCaseSheetMatchStatus(caseSheet1, caseSheet2));
	}
	
	@Test(groups= {"Regression","PracticeManagement"},priority=543)
	public synchronized void applyCaseSheetInConsultationPageAndVerify() throws Exception {
		logger = Reports.extent.createTest("EMR Select caseSheet in consultation page");
		VitalMasterTest vmt = new VitalMasterTest();
		ConsultationTest.UHID=SheetsAPI.getDataProperties(Web_Testbase.input+".patientUHID");
		pmt.clickOrgHeader();
		vmt.startConsultation();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, dp.moveToDiagnosisModule(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getGlobalPrintModule(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver,sp.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger) );
		Web_GeneralFunctions.click(sheet.getCaseSheetElementInConsultationPage(Login_Doctor.driver, logger), "click on CaseSheet in consultation page", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getSelectCaseSheetDesignerDropDown(Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(sheet.getSelectCaseSheetDesignerDropDown(Login_Doctor.driver, logger), "click on casesheet dropdown", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getSavedCaseSheetName(Login_Doctor.driver, templateName, logger));
		Web_GeneralFunctions.click(sheet.getSavedCaseSheetName(Login_Doctor.driver, templateName, logger), "clicking on previously saved casesheet", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(sheet.getCaseSheetSelectbuttonInConsultationPage(Login_Doctor.driver, logger), "click select button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		caseSheet1 = sheet.getSelectedCaseSheetExaminationNamesInConsultationPage(Login_Doctor.driver, logger);
		assertTrue(sheet.getCaseSheetMatchStatus(caseSheet1,caseSheet2));
	}
	
	public synchronized void clickSaveCaseSheetDesigner()throws Exception{
		logger = Reports.extent.createTest("EMR save case sheet designer");
		Web_GeneralFunctions.click(sheet.getSaveCaseSheetButton(Login_Doctor.driver, logger), "save case sheet designer", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getSaveCustomCaseSheetButton(Login_Doctor.driver, logger));	
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sheet.getSavePopUpCloseButton(Login_Doctor.driver, logger));
	}
}

