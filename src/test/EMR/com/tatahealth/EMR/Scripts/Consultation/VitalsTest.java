
package com.tatahealth.EMR.Scripts.Consultation;

import static org.testng.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.Consultation.LabResultPage;
import com.tatahealth.EMR.pages.Consultation.VitalsPage;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;
import com.tatahealth.ReusableModules.ReusableData;

public class VitalsTest {
	@BeforeClass(alwaysRun=true,groups= {"Regression","Appointment_Doctor"})
	public static void beforeClass() throws Exception {
		ConsultationTest currentTest = new ConsultationTest();
		currentTest.loginAndStartConsultation("Vitals");
	}
	
	@AfterClass(groups= {"Regression","Appointment_Doctor"})
	public static void afterClass() throws Exception {
		Login_Doctor.LogoutTest();
		System.out.println("Closed Login_Doctor.driver");
		Reports.extent.flush();
	}
	
	public List<String> savedVitals = new ArrayList<String>();
	public static Integer vital = 0;

	ExtentTest logger;
	
	public synchronized void clickSaveVitals() throws Exception {

		logger = Reports.extent.createTest("EMR  save vital module");
		VitalsPage vitals = new VitalsPage();

		Web_GeneralFunctions.click(vitals.saveVitals(Login_Doctor.driver, logger), "Save vitals by right menu",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
	}

	@Test(groups = { "Regression", "Login" }, priority = 672)
	public synchronized void moveToVitalModule() throws Exception {

		logger = Reports.extent.createTest("EMR move to vital module");
		VitalsPage vitals = new VitalsPage();

		Web_GeneralFunctions.click(vitals.moveToVitalModule(Login_Doctor.driver, logger), "Move to vital module",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
	}

	@Test(groups = { "Regression", "Login" }, priority = 673)
	public synchronized void enterInvalidBPValue() throws Exception {

		logger = Reports.extent.createTest("EMR enter vital above 300");
		VitalsPage vitals = new VitalsPage();

		Web_GeneralFunctions.sendkeys(vitals.getBPSysElement(Login_Doctor.driver, logger), "500", "Enter above 300",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
		String text = Web_GeneralFunctions.getText(vitals.getMessage(Login_Doctor.driver, logger), "Get waning message",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
		System.out.println("vital text : " + text);
		if (text.equalsIgnoreCase("Please enter a value between 0 and 300")) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}
		Thread.sleep(1000);
	}

	@Test(groups = { "Regression", "Login" }, priority = 674)
	public synchronized void enterCharacterAndSpecialCharactersInSysAndDia() throws Exception {
		logger = Reports.extent.createTest("EMR enter character and special character in BP Sys and dia");
		VitalsPage vitals = new VitalsPage();
		String sys = "ewj@#$%^<ds>{}':";

		Web_GeneralFunctions.sendkeys(vitals.getBPSysElement(Login_Doctor.driver, logger), sys, "Enter above 300",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.sendkeys(vitals.getBPDiaElement(Login_Doctor.driver, logger), sys, "Enter above 300",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
		clickSaveVitals();
		moveToVitalModule();
		String sysText = Web_GeneralFunctions.getAttribute(vitals.getBPSysElement(Login_Doctor.driver, logger), "value",
				"Get sys value", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		String diaText = Web_GeneralFunctions.getAttribute(vitals.getBPDiaElement(Login_Doctor.driver, logger), "value",
				"Get sys value", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		if (sysText.isEmpty() && diaText.isEmpty()) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}
		Thread.sleep(1000);
	}

	@Test(groups = { "Regression", "Login" }, priority =675)
	public synchronized void enterValidBPSys() throws Exception {

		logger = Reports.extent.createTest("EMR enter sys vital");
		VitalsPage vitals = new VitalsPage();
		Random r = new Random();
		vital = r.nextInt(120) + 1000;

		Web_GeneralFunctions.sendkeys(vitals.getBPSysElement(Login_Doctor.driver, logger), vital.toString(),
				"Enter BP sys value", Login_Doctor.driver, logger);
		Thread.sleep(1000);

	}

	@Test(groups = { "Regression", "Login" }, priority = 676)
	public synchronized void saveVitalsWithoutBPDia() throws Exception {
		logger = Reports.extent.createTest("EMR save vitals with out BP dia");
		VitalsPage vitals = new VitalsPage();
		clickSaveVitals();
		String text = Web_GeneralFunctions.getText(vitals.getJSErrorMessage(Login_Doctor.driver, logger),
				"Get js weaning message", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		System.out.println("bp : " + text);
		if (text.equalsIgnoreCase("Please enter a valid BP value")) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}
		Thread.sleep(1000);
	}

	@Test(groups = { "Regression", "Login" }, priority = 677)
	public synchronized void enterValidBPDia() throws Exception {

		logger = Reports.extent.createTest("EMR enter dia vital");
		VitalsPage vitals = new VitalsPage();
		Random r = new Random();
		Integer dia = r.nextInt(90) + 1000;

		Web_GeneralFunctions.sendkeys(vitals.getBPDiaElement(Login_Doctor.driver, logger), dia.toString(),
				"Enter BP Dia", Login_Doctor.driver, logger);
		Thread.sleep(1000);

		savedVitals.add("BP (mmHg)");
		String sys = vital.toString();
		String diaInString = dia.toString();
		String bp = sys.substring(0, sys.length() - 1) + "/" + diaInString.substring(0, diaInString.length() - 1);
		savedVitals.add(bp);
	}

	@Test(groups = { "Regression", "Login" }, priority = 678)
	public synchronized void enterInvalidSpo2Value() throws Exception {

		logger = Reports.extent.createTest("EMR enter spo2 vital above 100");
		VitalsPage vitals = new VitalsPage();

		Web_GeneralFunctions.sendkeys(vitals.getSpo2Element(Login_Doctor.driver, logger), "200", "Enter above 100",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
		String text = Web_GeneralFunctions.getText(vitals.getSpo2Message(Login_Doctor.driver, logger),
				"Get waning message", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		// System.out.println("vital text : "+text);
		if (text.equalsIgnoreCase("SpO2% value should be from 0 to 100")) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}
		Thread.sleep(1000);
	}

	@Test(groups = { "Regression", "Login" }, priority = 679)
	public synchronized void enterCharacterAndSpecialCharactersInSpo2() throws Exception {
		logger = Reports.extent.createTest("EMR enter character and special character in SpO2");
		VitalsPage vitals = new VitalsPage();
		String sys = "ewj@#$%^<ds>{}':";

		Web_GeneralFunctions.sendkeys(vitals.getSpo2Element(Login_Doctor.driver, logger), sys,
				"Enter characters and special characters in spo2", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		clickSaveVitals();
		moveToVitalModule();
		String sysText = Web_GeneralFunctions.getAttribute(vitals.getSpo2Element(Login_Doctor.driver, logger), "value",
				"Get spo2 value", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		if (sysText.isEmpty()) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}
		Thread.sleep(1000);
	}

	@Test(groups = { "Regression", "Login" }, priority = 680)
	public synchronized void enterSpo2Value() throws Exception {

		logger = Reports.extent.createTest("EMR enter spo2 vital");
		VitalsPage vitals = new VitalsPage();
		Random r = new Random();
		Integer spo = r.nextInt(10);
		spo += 90;

		Web_GeneralFunctions.sendkeys(vitals.getSpo2Element(Login_Doctor.driver, logger), spo.toString(), "Enter spo2",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
		savedVitals.add("SpO2%");
		savedVitals.add(spo.toString() + ".00");
	}

	@Test(groups = { "Regression", "Login" }, priority = 681)
	public synchronized void enterInvalidTemperatueeValue() throws Exception {

		logger = Reports.extent.createTest("EMR enter temperature vital above 115");
		VitalsPage vitals = new VitalsPage();

		Web_GeneralFunctions.sendkeys(vitals.getTemperatureElement(Login_Doctor.driver, logger), "200",
				"Enter above 115", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		String text = Web_GeneralFunctions.getText(vitals.getTemperatureMessage(Login_Doctor.driver, logger),
				"Get waning message", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		// System.out.println("vital text : "+text);
		if (text.equalsIgnoreCase("Please enter a value between 0 and 115")) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}
		Thread.sleep(1000);
	}

	@Test(groups = { "Regression", "Login" }, priority = 682)
	public synchronized void enterCharacterAndSpecialCharactersInTemperature() throws Exception {
		logger = Reports.extent.createTest("EMR enter character and special character in temperature");
		VitalsPage vitals = new VitalsPage();
		String sys = "ewj@#$%^<ds>{}':";

		Web_GeneralFunctions.sendkeys(vitals.getTemperatureElement(Login_Doctor.driver, logger), sys,
				"Enter characters and special characters in spo2", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		// clickSaveVitals();
		Web_GeneralFunctions.click(vitals.saveVitals(Login_Doctor.driver, logger), "Save vitals by right menu",
				Login_Doctor.driver, logger);

		moveToVitalModule();
		String sysText = Web_GeneralFunctions.getAttribute(vitals.getTemperatureElement(Login_Doctor.driver, logger),
				"value", "Get spo2 value", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		if (sysText.isEmpty()) {
			assertTrue(true);

		} else {
			assertTrue(false);
		}
		Thread.sleep(1000);
	}

	@Test(groups = { "Regression", "Login" }, priority = 683)
	public synchronized void enterTemperatureValue() throws Exception {

		logger = Reports.extent.createTest("EMR enter temperature vital");
		VitalsPage vitals = new VitalsPage();
		Random r = new Random();
		Integer dia = r.nextInt(15);
		dia += 95;

		Web_GeneralFunctions.sendkeys(vitals.getTemperatureElement(Login_Doctor.driver, logger), dia.toString(),
				"Enter temperature value", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		savedVitals.add("Temp (F)");
		savedVitals.add(dia.toString() + ".00");
	}

	@Test(groups = { "Regression", "Login" }, priority = 684)
	public synchronized void enterInvalidPulseRateValue() throws Exception {

		logger = Reports.extent.createTest("EMR enter temperature vital above 115");
		VitalsPage vitals = new VitalsPage();

		Web_GeneralFunctions.sendkeys(vitals.getPulseRateElement(Login_Doctor.driver, logger), "500", "Enter above 300",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
		String text = Web_GeneralFunctions.getText(vitals.getPulseRateMessage(Login_Doctor.driver, logger),
				"Get waning message", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		System.out.println("vital text : " + text);
		if (text.equalsIgnoreCase("Please enter a value between 0 and 300")) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}
		Thread.sleep(1000);
	}

	@Test(groups = { "Regression", "Login" }, priority = 685)
	public synchronized void enterCharacterAndSpecialCharactersInPulseRate() throws Exception {
		logger = Reports.extent.createTest("EMR enter character and special character in pulse rate");
		VitalsPage vitals = new VitalsPage();
		String sys = "ewj@#$%^<ds>{}':";

		Web_GeneralFunctions.sendkeys(vitals.getPulseRateElement(Login_Doctor.driver, logger), sys,
				"Enter characters and special characters in pulse rate", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		clickSaveVitals();
		moveToVitalModule();
		String sysText = Web_GeneralFunctions.getAttribute(vitals.getPulseRateElement(Login_Doctor.driver, logger),
				"value", "Get pulse rate value", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		if (sysText.isEmpty()) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}
		Thread.sleep(1000);
	}

	@Test(groups = { "Regression", "Login" }, priority = 686)
	public synchronized void enterPulseRateValue() throws Exception {

		logger = Reports.extent.createTest("EMR enter pulse rate vital");
		VitalsPage vitals = new VitalsPage();
		Random r = new Random();
		Integer dia = r.nextInt(10);
		dia += 70;

		Web_GeneralFunctions.sendkeys(vitals.getPulseRateElement(Login_Doctor.driver, logger), dia.toString(),
				"Enter pulse rate value", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		savedVitals.add("Pulse (bpm)");
		savedVitals.add(dia.toString() + ".00");
	}

	@Test(groups = { "Regression", "Login" }, priority = 687)
	public synchronized void saveVitals() throws Exception {

		logger = Reports.extent.createTest("EMR save vitals");
		VitalsPage vitals = new VitalsPage();
		clickSaveVitals();
		String text = Web_GeneralFunctions.getText(vitals.getVitalsSuccessMessage(Login_Doctor.driver, logger),
				"Get vitals success message", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		// System.out.println("saved vitals : "+text);
		if (text.equalsIgnoreCase("Vitals details have been saved successfully")) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}
		Thread.sleep(1000);
	}

/*	@Test(groups = { "Regression", "Login" }, priority = 877)
	public synchronized void saveVitalAndVerifyLength() throws Exception {

		logger = Reports.extent.createTest("EMR save vital and verify mex length 3");
		VitalsPage vitals = new VitalsPage();
		// clickSaveVitals();
		moveToVitalModule();
		String sysText = Web_GeneralFunctions.getAttribute(vitals.getBPSysElement(Login_Doctor.driver, logger), "value",
				"Get sys value", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		String diaText = Web_GeneralFunctions.getAttribute(vitals.getBPDiaElement(Login_Doctor.driver, logger), "value",
				"Get dia value", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		String spo2Text = Web_GeneralFunctions.getAttribute(vitals.getSpo2Element(Login_Doctor.driver, logger), "value",
				"Get spo2 value", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		String temperature = Web_GeneralFunctions.getAttribute(
				vitals.getTemperatureElement(Login_Doctor.driver, logger), "value", "Get temperature value",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
		System.out.println("sys : " + sysText.length() + " dia len : " + diaText.length() == 3 + " spo2 len : "
				+ spo2Text.length() + " temp len : " + temperature.length());
		if (sysText.length() == 3 && diaText.length() == 3 && spo2Text.length() < 3 && temperature.length() < 4) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}
	}*/

	@Test(groups = { "Regression", "Login" }, priority = 688)
	public synchronized void clickAddParameter() throws Exception {

		logger = Reports.extent.createTest("EMR add parameter vital");
		VitalsPage vitals = new VitalsPage();
		Web_GeneralFunctions.scrollElementByJavaScriptExecutor(vitals.getVitalsLabel(Login_Doctor.driver, logger),"Get vital notes label", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);

		Web_GeneralFunctions.click(vitals.getAddNewVitalParameter(Login_Doctor.driver, logger), "click add parameter",Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);

	}

	@Test(groups = { "Regression", "Login" }, priority = 689)
	public synchronized void selectParameterFromList() throws Exception {

		logger = Reports.extent.createTest("EMR add tooth eruption parameter vital");
		VitalsPage vitals = new VitalsPage();

		Web_GeneralFunctions.click(vitals.getToothEruptionParameter(Login_Doctor.driver, logger),"click add tooth eruption parameter", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.click(vitals.getHipGirthParameter(Login_Doctor.driver, logger),"click add hip girth parameter", Login_Doctor.driver, logger);
		Thread.sleep(1000);
	}

	@Test(groups = { "Regression", "Login" }, priority = 690)
	public synchronized void clickAdd() throws Exception {

		logger = Reports.extent.createTest("EMR add  vital");
		VitalsPage vitals = new VitalsPage();

		Web_GeneralFunctions.click(vitals.getAddFromParameterForm(Login_Doctor.driver, logger), "click add",Login_Doctor.driver, logger);
		Thread.sleep(1000);

	}

	@Test(groups = { "Regression", "Login" }, priority = 691)
	public synchronized void addedParameterPresentInVitalModule() throws Exception {

		logger = Reports.extent.createTest("EMR added parameter present in vital module");
		VitalsPage vitals = new VitalsPage();
		moveToVitalModule();
		String toothEruption = Web_GeneralFunctions.getText(vitals.getToothEruptionLabel(Login_Doctor.driver, logger),"Get tooth eruption label", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		String hipGirth = Web_GeneralFunctions.getText(vitals.getHipGirthLabel(Login_Doctor.driver, logger),"Get Hip gieth label", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		System.out.println("tooth : " + toothEruption + " hip : " + hipGirth);
		if (toothEruption.equalsIgnoreCase("Tooth Eruption") && hipGirth.equalsIgnoreCase("Hip Girth")) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}
		Thread.sleep(1000);
	}

	@Test(groups = { "Regression", "Login" }, priority = 692)
	public synchronized void enterValueInToothEruption() throws Exception {

		logger = Reports.extent.createTest("EMR enter value in tooth eruption");
		VitalsPage vitals = new VitalsPage();
		Random r = new Random();
		Integer tooth = r.nextInt(100);
		if (tooth == 0) {
			tooth += 30;
		}

		Web_GeneralFunctions.sendkeys(vitals.getToothEruptionElement(Login_Doctor.driver, logger), tooth.toString(),"enter tooth value", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		savedVitals.add("Tooth Erup");
		savedVitals.add(tooth.toString() + ".00");
	}

	// even after check out Hip Girth & Abdominal Girth is not going away/manual defect
	@Test(groups = { "Regression", "Login" }, priority = 693)
	public synchronized void saveVitalFromAddParametersChanges() throws Exception {

		logger = Reports.extent.createTest("EMR save vitals from add parameters");
		VitalsPage vitals = new VitalsPage();
		clickSaveVitals();
		moveToVitalModule();
		boolean toothEruptionLabel = vitals.isToothEruptionDisplayed(Login_Doctor.driver, logger);
		Thread.sleep(1000);
		boolean hipGirthLabel = vitals.isHipGirthDisplayed(Login_Doctor.driver, logger);
		Thread.sleep(1000);
		if (toothEruptionLabel == true && hipGirthLabel == false) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}
		Thread.sleep(1000);

	}

	@Test(groups = { "Regression", "Login" }, priority = 694)
	public synchronized void deleteNonEmptyField() throws Exception {

		logger = Reports.extent.createTest("EMR delete non empty field");
		VitalsPage vitals = new VitalsPage();
		clickAddParameter();

		Web_GeneralFunctions.click(vitals.getToothEruptionParameter(Login_Doctor.driver, logger),"Click Tooth eruption parameter", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		String text = Web_GeneralFunctions.getText(vitals.getNonEmptyFieldMessage(Login_Doctor.driver, logger),"Get message", Login_Doctor.driver, logger);
		if (text.equalsIgnoreCase("Trying to delete non-empty field !!")) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}
		Web_GeneralFunctions.wait(1);

	}
	
	  //this tc is became NA
	  
	  @Test(groups= {"Regression","Login"},priority=695) 
	  public synchronized void uncheckEmptyVitals()throws Exception{
	  
	  logger = Reports.extent.
	  createTest("EMR uncheck checked vitals from add parameter pop up");
	  VitalsPage vitals = new VitalsPage();
		clickAddParameter();

	  Web_GeneralFunctions.click(vitals.getHipGirthParameter(Login_Doctor.driver,
	  logger), "Click Hip girth parameter", Login_Doctor.driver, logger);
	  Thread.sleep(500); 
	  // after fix remove below line
	  Web_GeneralFunctions.click(vitals.getHipGirthParameter(Login_Doctor.driver,logger),"Click Hip girth parameter", Login_Doctor.driver, logger);
	  Thread.sleep(500); 

	  clickAdd(); Thread.sleep(500);
	  
	  boolean hipGirthLabel = vitals.isHipGirthDisplayed(Login_Doctor.driver,logger);
	  
	  if(hipGirthLabel == true) { assertTrue(true); }else { assertTrue(false); } }
	 

	@Test(groups = { "Regression", "Login" }, priority = 696)
	public synchronized void closeParameter() throws Exception {

		logger = Reports.extent.createTest("EMR close add parameter");
		VitalsPage vitals = new VitalsPage();

		Web_GeneralFunctions.click(vitals.getAddParameterClosePopUp(Login_Doctor.driver, logger),"Get close pop up add parameter", Login_Doctor.driver, logger);
		Thread.sleep(1000);

	}

	@Test(groups = { "Regression", "Login" }, priority = 697)
	public synchronized void enterValueInWeight() throws Exception {

		logger = Reports.extent.createTest("EMR enter value in tooth eruption");
		VitalsPage vitals = new VitalsPage();
		Random r = new Random();
		Integer tooth = r.nextInt(10);
		tooth += 50;
		Web_GeneralFunctions.clearWebElement(vitals.getWeightElement(Login_Doctor.driver, logger),"Clear already present value", Login_Doctor.driver, logger);
		Thread.sleep(1000);

		Web_GeneralFunctions.sendkeys(vitals.getWeightElement(Login_Doctor.driver, logger), tooth.toString(),"enter weight value", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		savedVitals.add("Wt (kg)");
		savedVitals.add(tooth.toString() + ".00");
	}

	@Test(groups = { "Regression", "Login" }, priority = 698)
	public synchronized void enterValueInHeight() throws Exception {

		logger = Reports.extent.createTest("EMR enter value in tooth eruption");
		VitalsPage vitals = new VitalsPage();
		Random r = new Random();
		Integer tooth = r.nextInt(10);
		tooth += 150;
		Web_GeneralFunctions.clearWebElement(vitals.getHeightElement(Login_Doctor.driver, logger),"Clear already present value", Login_Doctor.driver, logger);
		Thread.sleep(1000);

		Web_GeneralFunctions.sendkeys(vitals.getHeightElement(Login_Doctor.driver, logger), tooth.toString(),"enter height value", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		savedVitals.add("Ht (cm)");
		savedVitals.add(tooth.toString() + ".00");
	}

	@Test(groups = { "Regression", "Login" }, priority = 699)
	public synchronized void verifyBMI() throws Exception {

		logger = Reports.extent.createTest("EMR verify bmi");
		VitalsPage vitals = new VitalsPage();

		String bmi = Web_GeneralFunctions.getAttribute(vitals.getBMIElement(Login_Doctor.driver, logger), "value","Get automatically calculated BMI", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		if (!bmi.isEmpty()) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}
	}

	@Test(groups = { "Regression", "Login" }, priority = 700)
	public synchronized void enterVitalNotes() throws Exception {

		logger = Reports.extent.createTest("EMR enter vital notes");
		VitalsPage vitals = new VitalsPage();
		String freeText = RandomStringUtils.randomAlphabetic(15);

		Web_GeneralFunctions.sendkeys(vitals.getVitalNotesElement(Login_Doctor.driver, logger), freeText,"Enter vital notes", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		freeText = freeText.substring(0, 1).toUpperCase() + freeText.substring(1);
		savedVitals.add("Vital Notes : " + freeText);
		ReusableData.savedConsultationData.put("Vitals", (ArrayList<String>) savedVitals);
		System.out.println("Saved consultation data :  " + ReusableData.savedConsultationData.get("Vitals"));

	}

	@Test(groups = { "Regression", "Login" }, priority = 701)
	public synchronized void printAll() throws Exception {
		boolean vitalStatus = false;
		logger = Reports.extent.createTest("EMR vital in Print all pdf");
		VitalsPage vitals = new VitalsPage();
		GlobalPrintTest gp = new GlobalPrintTest();
		gp.moveToPrintModule();
		gp.printAll();
		String pdfContent = gp.getPDFPage();
		// System.out.println("pdf content : "+pdfContent);

		if (pdfContent.contains("Vitals")) {
			int i = 0;
			String value = "";
			while (i < savedVitals.size()) {
				value = savedVitals.get(i);
				// System.out.println("value of vitals : "+value);
				if (pdfContent.contains(value)) {
					vitalStatus = true;
				} else {
					vitalStatus = false;
					break;
				}
				i++;
			}

		} else {
			vitalStatus = false;
		}

		if (vitalStatus == true) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}

	}

	@Test(groups = { "Regression", "Login" }, priority = 702)
	public synchronized void verifyVitalsInPrintPDF() throws Exception {
		boolean vitalStatus = false;
		logger = Reports.extent.createTest("EMR vital in Print all pdf");
		VitalsPage vitals = new VitalsPage();
		GlobalPrintTest gp = new GlobalPrintTest();
		// gp.moveToPrintModule();
		gp.print();
		String pdfContent = gp.getPDFPage();
		System.out.println("pdf content... : " + pdfContent);

		if (pdfContent.contains("Vitals")) {
			int i = 0;
			String value = "";
			while (i < savedVitals.size()) {
				value = savedVitals.get(i);
				// System.out.println("value of vitals : "+value);
				if (pdfContent.contains(value)) {
					vitalStatus = true;
				} else {
					vitalStatus = false;
					break;
				}
				i++;
			}

		} else {
			vitalStatus = false;
		}

		if (vitalStatus == true) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}
		ReusableData.savedConsultationData.put("Vitals", (ArrayList<String>) savedVitals);

	}

	
	
	// after check out
	
	// add checkout code here
	
	//@Test(groups= {"Regression","Login"},priority=892)
	public synchronized void verifyVitals()throws Exception{
		logger = Reports.extent.createTest("EMR verify Vitals");
		  VitalsPage vitals = new VitalsPage();

		String weightText = Web_GeneralFunctions.getAttribute(vitals.getWeightElement(Login_Doctor.driver, logger), "value", "get weight field", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		
		String heightText = Web_GeneralFunctions.getAttribute(vitals.getHeightElement(Login_Doctor.driver, logger), "value", "get height field", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		//boolean toothEruptionLabel = vitals.isToothEruptionDisplayed(Login_Doctor.driver, logger);

		if (!weightText.isEmpty() && !heightText.isEmpty()/* && toothEruptionLabel==true */) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		
		
		// below line code to reset the ToothEruption.So script will not fail on the next run
		vitals.getWeightElement(Login_Doctor.driver, logger).clear();
		vitals.getHeightElement(Login_Doctor.driver, logger).clear();
		//vitals.getToothEruptionElement(Login_Doctor.driver, logger).clear();;
		saveVitals();
		
	}
	
	
	
	
	
	
}
