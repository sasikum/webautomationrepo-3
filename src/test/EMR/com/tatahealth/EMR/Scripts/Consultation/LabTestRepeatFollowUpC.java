package com.tatahealth.EMR.Scripts.Consultation;
import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.Consultation.LabPeriodicityPages;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class LabTestRepeatFollowUpC {
	public static LabPeriodicityPages lab = new LabPeriodicityPages();
	public static ConsultationTest consult = new ConsultationTest();
	public static ExtentTest logger;
	public static String patientName = "Maha";
	public static Integer availableSlot = 0;
	public static String prevSlotId = "";
	public static String slotId = "";
	
	@BeforeClass(alwaysRun = true)
	public static void beforeClass() throws Exception {
		Reports.reports();
		Login_Doctor.executionName = "Lab Test Periodicity";
		Login_Doctor.LoginTestwithDiffrentUser("Kasu");
		consult.labStartConsultation("Automation");
		Web_GeneralFunctions.scrollElementByJavaScriptExecutor(lab.getLabTest(Login_Doctor.driver, logger), "Scroll page till Lab test is visible on the page", 
				Login_Doctor.driver, logger);
	}
	@AfterClass(alwaysRun = true)
	public static void afterClass() throws Exception {
		Login_Doctor.driver.manage().deleteAllCookies();
		Web_GeneralFunctions.scrollDown(Login_Doctor.driver);
		Login_Doctor.LogoutTest();
		Reports.extent.flush();
	}	
	@Test(priority = 11, groups = { "Regression", "LabPeriodicity" }, description = "LT_31")
	public void repeat_follow_up_C() throws Exception {
		logger = Reports.extent.createTest("For the new lab test C prescribed there should not be pre-populated values present in the textfields");
		String number = RandomStringUtils.randomNumeric(1);
		JavascriptExecutor js = (JavascriptExecutor)Login_Doctor.driver;
		String labTamplateText = "EMR - Lab Periodicity";
		lab.getlabTestTamplateSearch(Login_Doctor.driver, logger).clear();
		lab.getlabTestTamplateSearch(Login_Doctor.driver, logger).sendKeys(labTamplateText);
		Web_GeneralFunctions.wait(2);
		js.executeScript("arguments[0].click()", lab.getSearchTestOrScan(Login_Doctor.driver, logger));
		js.executeScript("arguments[0].value='Urine Analysis';", lab.getSearchTestOrScan(Login_Doctor.driver, logger));
		lab.getNotesTestOrScan(Login_Doctor.driver, logger).clear();
		lab.getEveryTextfield(Login_Doctor.driver, logger).clear();
		lab.getNotesTestOrScan(Login_Doctor.driver, logger).sendKeys(labTamplateText);
		lab.getEveryTextfield2(Login_Doctor.driver, logger).sendKeys(number);
		Select textDropdown = new Select(lab.getEveryDropdown(Login_Doctor.driver, logger));
		textDropdown.selectByIndex(2);
		Web_GeneralFunctions.wait(3);
		lab.getForTextfield(Login_Doctor.driver, logger).clear();
		lab.getForTextfield(Login_Doctor.driver, logger).sendKeys(number);
		Select forTextDropdown = new Select(lab.getForDropdown(Login_Doctor.driver, logger));
		forTextDropdown.selectByIndex(3);
		Web_GeneralFunctions.wait(3);
		//lab.getPlusIcon(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(3);
		lab.getSearchTestOrScan2(Login_Doctor.driver, logger).clear();
		js.executeScript("arguments[0].value='Fasting Blood Sugar (FBS)';", lab.getSearchTestOrScan2(Login_Doctor.driver, logger));
		lab.getNotesTestOrScan2(Login_Doctor.driver, logger).clear();
		lab.getNotesTestOrScan2(Login_Doctor.driver, logger).sendKeys(labTamplateText);
		lab.getEveryTextfield2(Login_Doctor.driver, logger).sendKeys(number);
		Select textDropdown2 = new Select(lab.getEveryDropdown2(Login_Doctor.driver, logger));
		textDropdown2.selectByIndex(2);
		lab.getForTextfield2(Login_Doctor.driver, logger).sendKeys(number);
		Select forTextDropdown2 = new Select(lab.getForDropdown2(Login_Doctor.driver, logger));
		forTextDropdown2.selectByIndex(3);
		lab.getPlusIcon(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(3);
		Assert.assertTrue(lab.getSearchTestOrScanLast(Login_Doctor.driver, logger).getAttribute("value").isEmpty());
		Assert.assertTrue(lab.getNotesTestOrScanLast(Login_Doctor.driver, logger).getAttribute("value").isEmpty());
	}
	@Test(priority = 12, groups = { "Regression", "LabPeriodicity" }, description = "LT_32, LT_33", dependsOnMethods = "repeat_follow_up_C")
	public void update_values_on_c() throws Exception {
		logger = Reports.extent.createTest("To verify doctor can able to enter the values in the textfields for new lab test C");
		String number = RandomStringUtils.randomNumeric(2);
		JavascriptExecutor js = (JavascriptExecutor)Login_Doctor.driver;
		String labTamplateText = "EMR Automation Testing";
		Web_GeneralFunctions.wait(2);
		js.executeScript("arguments[0].value='Uric Acid';", lab.getSearchTestOrScan3(Login_Doctor.driver, logger));
		//lab.getNotesTestOrScan3(Login_Doctor.driver, logger).sendKeys(labTamplateText);
		lab.getEveryTextfield3(Login_Doctor.driver, logger).sendKeys(number);
		Select textDropdown3 = new Select(lab.getEveryDropdown3(Login_Doctor.driver, logger));
		textDropdown3.selectByIndex(2);
		Web_GeneralFunctions.wait(3);
		lab.getForTextfield3(Login_Doctor.driver, logger).sendKeys(number);
		Select forTextDropdown3 = new Select(lab.getForDropdown3(Login_Doctor.driver, logger));
		forTextDropdown3.selectByIndex(3);
		lab.getPlusIcon(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(3);
		Assert.assertEquals(lab.getSearchTestOrScan3(Login_Doctor.driver, logger).getAttribute("value"), "Uric Acid");
		System.out.println("======================================>"+lab.getNotesTestOrScan3(Login_Doctor.driver, logger).getAttribute("value"));
		Assert.assertTrue(labTamplateText.contains(lab.getNotesTestOrScan3(Login_Doctor.driver, logger).getText()));
		Web_GeneralFunctions.scrollDown(Login_Doctor.driver);
		Web_GeneralFunctions.wait(3);
		Assert.assertEquals(lab.getSearchTestOrScan3(Login_Doctor.driver, logger).getAttribute("value"), "Uric Acid");
		Assert.assertTrue(labTamplateText.contains(lab.getNotesTestOrScan3(Login_Doctor.driver, logger).getText()));
	}
}