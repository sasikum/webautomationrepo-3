
package com.tatahealth.EMR.Scripts.Consultation;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.Consultation.SymptomPage;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;
import com.tatahealth.ReusableModules.DownloadPDF;
import com.tatahealth.ReusableModules.ReusableData;
import com.tatahealth.ReusableModules.reusableModules;

public class SymptomTest {
	
	@BeforeClass(alwaysRun=true,groups= {"Regression","Appointment_Doctor"})
	public static void beforeClass() throws Exception {
		ConsultationTest currentTest = new ConsultationTest();
		currentTest.loginAndStartConsultation("Symptoms");
	}
	
	@AfterClass(groups= {"Regression","Appointment_Doctor"})
	public static void afterClass() throws Exception {
		Login_Doctor.LogoutTest();
		System.out.println("Closed Login_Doctor.driver");
		Reports.extent.flush();
	}
	
	public static Integer row = 0;
	public List<String> savedSymptoms = new ArrayList<String>();
	
	ExtentTest logger;
	//public static boolean deleteSavedSymptom  = false;
	
	 
	public synchronized void clickAddSymptoms()throws Exception{
		
		logger = Reports.extent.createTest("EMR click add Symptoms");
		SymptomPage symptom = new SymptomPage();
		Web_GeneralFunctions.click(symptom.addSymptom(Login_Doctor.driver, logger), "click add symtom", Login_Doctor.driver, logger);
		Thread.sleep(1000);
	}

  // entering symptom duration and other details
	@Test(groups= {"Regression","Login"},priority=640)
	public synchronized void Consultation_001()throws Exception{
		
		logger = Reports.extent.createTest("EMR Symptoms");
		System.out.println("******************************system started *************************************");
		//Web_GeneralFunctions generalFunctions = new Web_GeneralFunctions();
		
		SymptomPage symptom = new SymptomPage();
		String duration = "5day$";
		String otherDetails = "Vomit**";
		Web_GeneralFunctions.click(symptom.selectSymptomFromDropDown(Login_Doctor.driver,row,"",logger), "select symptom from drop down", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.sendkeys(symptom.getDuration(Login_Doctor.driver,row, logger),duration,"Sending duration value to text box", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.sendkeys(symptom.getOtherDetails(Login_Doctor.driver,row, logger), otherDetails, "Sending duration value to text box", Login_Doctor.driver, logger);
		Thread.sleep(3000);
		
		
	}
	
	// add one more row and select a value from drop down
	@Test(groups= {"Regression","Login"},priority=641)
	public synchronized void Consultation_002() throws Exception{
		
		row++;
		logger = Reports.extent.createTest("EMR Add button Symptoms");
		String searchSymptom = "Ast";
		
		SymptomPage symptom = new SymptomPage();
		
		Web_GeneralFunctions.click(symptom.addSymptom(Login_Doctor.driver,logger),"click add button to add more than one symptom", Login_Doctor.driver, logger);
		Thread.sleep(1000);
	
		Web_GeneralFunctions.click(symptom.selectSymptomFromDropDown(Login_Doctor.driver, row,searchSymptom, logger),"Select symptom from search list ", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		
	}
	
	// mandatory field validation of symptom name and error message validation
	@Test(groups= {"Regression","Login"},priority=642)
	public synchronized void Consultation_003() throws Exception{
		
		row++;
		logger = Reports.extent.createTest("EMR Save Symptoms with out adding value in symptom field");
		String duration = "4days";
		SymptomPage symptom = new SymptomPage();
		Web_GeneralFunctions.click(symptom.addSymptom(Login_Doctor.driver,logger),"click add button to add more than one symptom", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		Web_GeneralFunctions.sendkeys(symptom.getDuration(Login_Doctor.driver,row, logger),duration,"Sending duration value to text box", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		Web_GeneralFunctions.click(symptom.saveSymptom(Login_Doctor.driver, logger), "Save Symptoms by right menu", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		String text = Web_GeneralFunctions.getText(symptom.getJSErrorPopup(Login_Doctor.driver, logger), "Symptom mandatory error popup", Login_Doctor.driver, logger);
		if(text.equalsIgnoreCase("Symptom name is mandatory")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		
		Web_GeneralFunctions.wait(2);
	}
	//delete button
	@Test(groups= {"Regression","Login"},priority=643)
	public synchronized void Consultation_004()throws Exception{
		
		logger = Reports.extent.createTest("EMR Delete button Symptoms");
		SymptomPage symptom = new SymptomPage();
		Web_GeneralFunctions.click(symptom.getDeleteButtonBeforeSaving(Login_Doctor.driver, row, logger), "Click Delete button", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		
	}
	
	// save symptom and save message validation 
	@Test(groups= {"Regression","Login"},priority=644)
	public synchronized void Consultation_005() throws Exception{
		
		logger = Reports.extent.createTest("EMR Save Symptoms By Right Menu");
		
		SymptomPage symptom = new SymptomPage();
	
		Web_GeneralFunctions.click(symptom.saveSymptom(Login_Doctor.driver, logger), "Save Symptoms by right menu", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		
		String text = Web_GeneralFunctions.getText(symptom.getSuccessMessage(Login_Doctor.driver, logger), "get success text", Login_Doctor.driver, logger);
		if(text.equalsIgnoreCase("Vitals details have been saved successfully")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		Thread.sleep(2000);
		
	}
	
	//@Test(groups= {"Regression","Login"},priority=8)
	public synchronized void moveToSymtpomModule() throws Exception{
		
		logger = Reports.extent.createTest("EMR Move To Symptoms By Right Menu");
		SymptomPage symptom = new SymptomPage();
		Web_GeneralFunctions.click(symptom.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger), "Move to symptom module", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);
	}
	//duplicate sysmtoms
	@Test(groups= {"Regression","Login"},priority=645)
	public synchronized void Consultation_006()throws Exception{
		
		logger = Reports.extent.createTest("EMR Save Duplicate Symptoms");
		moveToSymtpomModule();
		SymptomPage symptom = new SymptomPage();
		Web_GeneralFunctions.click(symptom.addSymptom(Login_Doctor.driver, logger), "Add new row", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		String attr = Web_GeneralFunctions.getAttribute(symptom.getSymptom(Login_Doctor.driver, 1, logger), "value", "Get value from attribute", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		row++;
		Web_GeneralFunctions.sendkeys(symptom.getSymptom(Login_Doctor.driver, row, logger), attr, "Pass duplicate value", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		Web_GeneralFunctions.click(symptom.saveSymptom(Login_Doctor.driver, logger), "Save Symptoms by right menu", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		
		String text = Web_GeneralFunctions.getText(symptom.getSaveMessage(Login_Doctor.driver, logger), "Get Save Message", Login_Doctor.driver, logger);
		if(text.equalsIgnoreCase("Duplicate Symptom(s) found")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		Thread.sleep(2000);
		
	}

	//clear symptom
	@Test(groups= {"Regression","Login"},priority=646)
	public synchronized void Consultation_007()throws Exception{
		
		logger = Reports.extent.createTest("EMR Clear Symptom");
		SymptomPage symptom = new SymptomPage();
		//deleteRow();
		Web_GeneralFunctions.clearWebElement(symptom.getSymptom(Login_Doctor.driver, row, logger),"clear the symptom", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		
	}
	
	@Test(groups= {"Regression","Login"},priority=647)
	public synchronized void Consultation_008()throws Exception{
		
		logger = Reports.extent.createTest("EMR Save Symptoms By Scroll");
		SymptomPage symptom = new SymptomPage();
		Web_GeneralFunctions.click(symptom.saveSymptom(Login_Doctor.driver, logger), "Save Symptoms by scroll", Login_Doctor.driver, logger);
		Thread.sleep(2000);
		
		String text = Web_GeneralFunctions.getText(symptom.getSaveMessage(Login_Doctor.driver, logger), "Get Save Message", Login_Doctor.driver, logger);
		if(text.equalsIgnoreCase("Vitals details have been saved successfully")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		Thread.sleep(3000);
		
	}
	//printall
	@Test(groups= {"Regression","Login"},priority=648)
	public synchronized void Consultation_009()throws Exception{
		boolean status = false;
		
		logger = Reports.extent.createTest("EMR Print All PDF Check");
		GlobalPrintTest gpt = new GlobalPrintTest();
		SymptomPage symptom = new SymptomPage();
		gpt.moveToPrintModule();
		Web_GeneralFunctions.wait(1);
		gpt.printAll();
		String pdfContent = gpt.getPDFPage();
		System.out.println(".......pdfContent" + pdfContent);
		
		Web_GeneralFunctions.click(symptom.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger), "Move to symptom module", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);
		
		Web_GeneralFunctions.click(symptom.getSymptom(Login_Doctor.driver, 1, logger), "move to 1st row", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		
		int currentRow = row-1;
		String savedSymptom="";
		String savedSymptomDuration = "";
		String savedSymptomOther = "";

		int verifyRow = 1;
		
		if(pdfContent.contains("Symptoms")) {
			int i =0;
			
			while(verifyRow<=currentRow) {
				savedSymptom = Web_GeneralFunctions.getAttribute(symptom.getSymptom(Login_Doctor.driver, verifyRow, logger), "value", "Get symptom value", Login_Doctor.driver, logger);
				String duration = Web_GeneralFunctions.getAttribute(symptom.getDuration(Login_Doctor.driver, verifyRow, logger), "value", "Get Duration value", Login_Doctor.driver, logger);
				if(!duration.isEmpty()) {
					savedSymptom += ", Duration: "+duration;
					//savedSymptomDuration += " - "+duration;
				}
				String other = Web_GeneralFunctions.getAttribute(symptom.getOtherDetailValue(Login_Doctor.driver, verifyRow, logger), "value", "Get Other Detail value", Login_Doctor.driver, logger);
				if(!other.isEmpty()) {
					savedSymptom += ", Details : "+other;
					//savedSymptomOther += other;

				}

				if(pdfContent.contains(savedSymptom)&& pdfContent.contains(other)) {
					status = true;
				}else {
					status = false;
					break;
				}
				verifyRow++;
			}
			
		}else {
			assertTrue(false);
		}
		
		if(status == true) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		
		Web_GeneralFunctions.wait(2);
	}
	
	
	@Test(groups= {"Regression","Login"},priority=649)
	public synchronized void Consultation_010()throws Exception{
		
		System.out.println("consultation_0101");
		logger = Reports.extent.createTest("EMR Free Text Symptoms");
		SymptomPage symptom = new SymptomPage();
		String Duration = RandomStringUtils.randomNumeric(1)+RandomStringUtils.randomAlphabetic(4);
		Random r = new Random();
		int size = r.nextInt(20);
		size+=10;
		String freetext = RandomStringUtils.randomAlphabetic(size);
		Web_GeneralFunctions.click(symptom.addSymptom(Login_Doctor.driver, logger), "Add new row", Login_Doctor.driver, logger);
		Thread.sleep(2000);
		System.out.println("after add symptom method");
		//Web_GeneralFunctions.sendkeys(symptom.getSymptom(Login_Doctor.driver, row, logger),freetext,"Pass free text", Login_Doctor.driver, logger);
		List<WebElement> ele = symptom.getNoMatchingSymptom(freetext, row, Login_Doctor.driver, logger);
		Thread.sleep(1000);
		if(ele.size() == 1) {
			String message = ele.get(0).getText();
			if(message.equalsIgnoreCase("No matching Symptom found")) {
				assertTrue(true);
			}else {
				assertTrue(false);
			}
		}else {
			assertTrue(false);
		}
		
		Web_GeneralFunctions.sendkeys(symptom.getDuration(Login_Doctor.driver, row, logger), Duration,"Sending Duration value", Login_Doctor.driver, logger);
		Thread.sleep(2000);
		
		System.out.println("enter 3 symptom row");
		
	}
	
	//print
		@Test(groups= {"Regression","Login"},priority=650)
		public synchronized void Consultation_011()throws Exception{
			boolean status = false;
			
			logger = Reports.extent.createTest("EMR Print PDF Check");
			GlobalPrintTest gpt = new GlobalPrintTest();
			SymptomPage symptom = new SymptomPage();
			gpt.moveToPrintModule();
			Thread.sleep(1000);
			gpt.print();
			String pdfContent = gpt.getPDFPage();
			System.out.println("pdfContent.........."+ pdfContent);
			Web_GeneralFunctions.click(symptom.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger), "Move to symptom module", Login_Doctor.driver, logger);
			Thread.sleep(2000);
			
			Web_GeneralFunctions.click(symptom.getSymptom(Login_Doctor.driver, 1, logger), "move to 1st row", Login_Doctor.driver, logger);
			Thread.sleep(1000);
			
			int currentRow = row-1;
			String savedSymptom = "";
			String savedSymptomDuration = "";
			String savedSymptomOther = "";
			int verifyRow = 1;
			
			if(pdfContent.contains("Symptoms")) {
			
				while(verifyRow<=currentRow) {
					savedSymptomDuration = Web_GeneralFunctions.getAttribute(symptom.getSymptom(Login_Doctor.driver, verifyRow, logger), "value", "Get symptom value", Login_Doctor.driver, logger);
					String duration = Web_GeneralFunctions.getAttribute(symptom.getDuration(Login_Doctor.driver, verifyRow, logger), "value", "Get Duration value", Login_Doctor.driver, logger);
					System.out.println("--------------------------"+duration);
					if(!duration.isEmpty()) {
						savedSymptom += ", Duration: "+duration;
						//savedSymptom += duration;
						savedSymptomDuration +=  " - "+duration;


					}
					String other = Web_GeneralFunctions.getAttribute(symptom.getOtherDetailValue(Login_Doctor.driver, verifyRow, logger), "value", "Get Other Detail value", Login_Doctor.driver, logger);
					if(!other.isEmpty()) {
						savedSymptom += ", Details : "+other;
						savedSymptomOther += other;

					}
					
					if(pdfContent.contains(savedSymptom)&& pdfContent.contains(savedSymptomOther)) {
						status = true;
					}else {
						status = false;
						break;
					}
					verifyRow++;
				}
				
			}else {
				assertTrue(false);
			}
			
			if(status == true) {
				assertTrue(true);
			}else {
				assertTrue(false);
			}
			
			
			Thread.sleep(2000);
		}
		

		@Test(groups= {"Regression","Login"},priority=651)
		public synchronized void Consultation_012()throws Exception{
			
			row++;
			logger = Reports.extent.createTest("EMR Free Text Symptoms");
			SymptomPage symptom = new SymptomPage();
			String OtherDetails = RandomStringUtils.randomAlphabetic(10);
			moveToSymtpomModule();
			String searchString = "tio";
			Web_GeneralFunctions.click(symptom.addSymptom(Login_Doctor.driver, logger), "Add new row", Login_Doctor.driver, logger);
			Thread.sleep(1000);
			Web_GeneralFunctions.click(symptom.selectSymptomFromDropDown(Login_Doctor.driver, row, searchString, logger), "Search and select", Login_Doctor.driver, logger);
			Thread.sleep(2000);
			Web_GeneralFunctions.sendkeys(symptom.getOtherDetails(Login_Doctor.driver,row, logger), OtherDetails, "Sending other detail value to text box", Login_Doctor.driver, logger);
			Thread.sleep(2000);
			//savedSymptoms.add(symptomText+", Details : ");
			Consultation_005();
		}
		
		
		//write code for getting saved values and delete saved symptoms
		public synchronized void getSavedSymptomData() throws Exception{
			
			logger = Reports.extent.createTest("EMR Get Saved Symptom Data");
			
			moveToSymtpomModule();
			
			SymptomPage symptom = new SymptomPage();
			
			int currentRow = row;
			String savedSymptom = "";
			List<String> symptomArr = new ArrayList<String>();
			int verifyRow = 1;
			
			while(verifyRow<=currentRow) {
				savedSymptom = Web_GeneralFunctions.getAttribute(symptom.getSymptom(Login_Doctor.driver, verifyRow, logger), "value", "Get symptom value", Login_Doctor.driver, logger);
				String duration = Web_GeneralFunctions.getAttribute(symptom.getDuration(Login_Doctor.driver, verifyRow, logger), "value", "Get Duration value", Login_Doctor.driver, logger);
				if(!duration.isEmpty()) {
					savedSymptom += ", Duration: "+duration;
				}
				String other = Web_GeneralFunctions.getAttribute(symptom.getOtherDetailValue(Login_Doctor.driver, verifyRow, logger), "value", "Get Other Detail value", Login_Doctor.driver, logger);
				if(!other.isEmpty()) {
					savedSymptom += ", Details : "+other;
				}
				symptomArr.add(savedSymptom);
				verifyRow++;
			}
			ReusableData.savedConsultationData.put("Symptoms", (ArrayList<String>) symptomArr);
			System.out.println("Saved consultation data :  "+ReusableData.savedConsultationData.get("Symptoms"));
		}
		
		
		@Test(groups= {"Regression","Login"},priority=652)
		public synchronized void Consultation_013()throws Exception{
			logger = Reports.extent.createTest("EMR Delete Saved symptom");
			
			moveToSymtpomModule();
			SymptomPage symptom = new SymptomPage();
			Random r = new Random();
			Integer randomRow = r.nextInt(3);
			if(randomRow == 0) {
				randomRow +=1;
			}
			
			String deletedSymptom = Web_GeneralFunctions.getAttribute(symptom.getSymptom(Login_Doctor.driver, randomRow, logger), "value", "Get Symptom Data", Login_Doctor.driver, logger);
			Thread.sleep(1000);
		//	System.out.println("deleted symptom : "+deletedSymptom + "random row : "+randomRow);
			
			//delete the saved symptom
			Web_GeneralFunctions.click(symptom.getDeleteButtonAfterSaving(Login_Doctor.driver, randomRow, logger), "Click Delete button", Login_Doctor.driver, logger);
			Thread.sleep(1000);
			//save the changes
			Consultation_005();
			//deleted one row....so -1 from row field;
			row -=1;
			
			//get the saved data
			getSavedSymptomData();
			List<String> symptomsBeforeDeletiom = ReusableData.savedConsultationData.get("Symptoms");
			String verification = "";
			int i =0;
			while(i<symptomsBeforeDeletiom.size()) {
				verification += symptomsBeforeDeletiom.get(i);
				i++;
			}
			
			//System.out.println("verification symtpom : "+verification);
			if(!verification.contains(deletedSymptom)) {
				assertTrue(true);
			}else {
				assertTrue(false);
			}
			System.out.println("******************************system ended *************************************");

		}
		
	}
