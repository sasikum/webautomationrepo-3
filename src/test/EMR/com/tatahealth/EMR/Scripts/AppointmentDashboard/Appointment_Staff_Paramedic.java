package com.tatahealth.EMR.Scripts.AppointmentDashboard;

import java.util.Date;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Login.Login_Staff_Paramedic;
import com.tatahealth.EMR.pages.AppointmentDashboard.AllSlotsPage;
import com.tatahealth.EMR.pages.AppointmentDashboard.AppointmentsPage;
import com.tatahealth.EMR.pages.AppointmentDashboard.ConsultationPage;
import com.tatahealth.EMR.pages.AppointmentDashboard.PreConsultationPage;
import com.tatahealth.EMR.pages.CommonPages.CommonPage;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class Appointment_Staff_Paramedic {
	
	ExtentTest logger;
	public static int k=1;
	static WebDriver driver;
	
	@BeforeClass(groups= {"Regression","Appointment_Digitizer"})
	public static void beforeClass() throws Exception {
		Login_Staff_Paramedic.LoginTest();
		System.out.println("Initialized Driver");
		driver = Login_Staff_Paramedic.driver;
	}
	
	@AfterClass(groups= {"Regression","Appointment_Digitizer"})
	public static void afterClass() throws Exception {
		Login_Staff_Paramedic.LogoutTest();
		System.out.println("Closed Driver");
	} 
	
	
	/***
	 * @author Bala Yaswanth
	 * @throws Exception
	 * Check Whether Date is defaulted to today in All Slots Page
	 */
	@Test(priority=51,groups= {"Regression","Appointment_Digitizer"})
	public synchronized void APP_STA_PAR_001() throws Exception {
		Thread.sleep(3000);
		logger = Reports.extent.createTest("APP_STA_PAR_001");
		AppointmentsPage appoiPage = new AppointmentsPage();
		String EMRDate = appoiPage.getCurrentDate(driver, logger).getText();
		Date date = new Date();
		String localDate = date.toString().substring(8, 10);
		Assert.assertEquals(localDate, EMRDate, "Date is not Correct. Please Check");
		
		AllSlotsPage ASpage = new AllSlotsPage();
		while(ASpage.checkTimings(driver, logger)) {
			k++;
			ASpage.getDoctorWithTimings(k, driver, logger).click();
			Thread.sleep(10000);
			System.out.println(k);
			
		}
		
	}
	
	
	
	
	
	/***
	 * @author Bala Yaswanth
	 * @throws Exception
	 * Booking Appointment - Reschedule + Reschedule rescheduled appointment + cancel Appointment
	 */
	
	//@Test(priority=53,groups= {"Regression","Appointment_Digitizer"})
	public synchronized void APP_STA_PAR_003() throws Exception {
		
		
		logger = Reports.extent.createTest("APP_STA_PAR_003");
		
		
		
		AllSlotsPage ASpage = new AllSlotsPage();
		
		
		
		//Getting Available Slot Code
		int n = ASpage.getAvailableSlot(driver, logger);
		System.out.println(n);
		String slotId = ASpage.getSlotId(n, driver, logger);
		
		//Scrolling to available slot and booking appointment
		ScrollAndBookAllSlots(slotId);
		
		
		//Rescheduling appointment to next slot
		Web_GeneralFunctions.scrollElementToCenter(ASpage.getAppointmentDropDown(n, driver, logger), driver);
		Thread.sleep(6000);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(n, driver, logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(n, 2, driver, logger),"Clicking on Reschedule in Dropdown", driver, logger);
		//generalFunctions.click(ASpage.getReschedulefromDropdown(n, driver, logger),"Clicking on Reschedule in Dropdown", driver, logger);
		Web_GeneralFunctions.click(ASpage.getTimingsfromRescheduleAlert(driver, logger), "Clicking to get timings in Reschedule Popup", driver, logger);
		Web_GeneralFunctions.click(ASpage.setTimingsinRescheduleAlert((n+2), driver, logger), "Clicking to select next slot timings in Reschedule Popup", driver, logger);
		Web_GeneralFunctions.click(ASpage.getOKBtnfromRescheduleAlert(driver, logger), "Clicking on Ok in Popup", driver, logger);
		Thread.sleep(10000);
		
		
		//Rescheduling the Rescheduled Appointment
		n=n+1;
		Web_GeneralFunctions.scrollElementToCenter(ASpage.getAppointmentDropDown(n, driver, logger), driver);
		Thread.sleep(6000);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(n, driver, logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(n, 2, driver, logger),"Clicking on Reschedule in Dropdown", driver, logger);
		//generalFunctions.click(ASpage.getReschedulefromDropdown(n, driver, logger),"Clicking on Reschedule in Dropdown", driver, logger);
		Web_GeneralFunctions.click(ASpage.getTimingsfromRescheduleAlert(driver, logger), "Clicking to get timings in Reschedule Popup", driver, logger);
		Web_GeneralFunctions.click(ASpage.setTimingsinRescheduleAlert((n+2), driver, logger), "Clicking to select next slot timings in Reschedule Popup", driver, logger);
		Web_GeneralFunctions.click(ASpage.getOKBtnfromRescheduleAlert(driver, logger), "Clicking on Ok in Popup", driver, logger);
		Thread.sleep(10000);
		
		
		//Canceling the Rescheduled Appointment
		n=n+1;
		Web_GeneralFunctions.scrollElementToCenter(ASpage.getAppointmentDropDown(n, driver, logger), driver);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(n, driver, logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(n,3, driver, logger), "Clicking on Reschedule in Dropdown", driver, logger);
		Web_GeneralFunctions.click(ASpage.getReasondropdownfromCancelAlert(driver, logger), "Clicking to get reasons in Cancel Alert", driver, logger);
		Web_GeneralFunctions.click(ASpage.setReasoninCancelAlert(driver, logger),"Clicking on reasons in Cancel Alert", driver, logger);
		Web_GeneralFunctions.click(ASpage.getSubmitBtnfromCancelAlert(driver, logger), "Clicking on Submit in Alert", driver, logger);
		Thread.sleep(6000);
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/***
	 * @author Bala Yaswanth
	 * @throws Exception
	 * BA + CI + Pre consulting + preconsulted - Consultation
	 */
	//@Test(priority=54,groups= {"Regression","Appointment_Digitizer"})
	public synchronized void APP_STA_PAR_004() throws Exception {
		
		logger = Reports.extent.createTest("APP_STA_PAR_004");
		
		
		
		AllSlotsPage ASpage = new AllSlotsPage();
		CommonPage CPage = new CommonPage();
		PreConsultationPage PreconPage = new PreConsultationPage();
		
		//Getting Available Slot Code
		int n = ASpage.getAvailableSlot(driver, logger);
		String slotId = ASpage.getSlotId(n, driver, logger);
		
		//Scrolling to available slot and booking appointment
		ScrollAndBookAllSlots(slotId);
		
		//Checking in booked appointment
		Web_GeneralFunctions.scrollElementToCenter(ASpage.getAppointmentDropDown(n, driver, logger), driver);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(n, driver, logger), "Clicking to get Appointment Dropdown", driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(n,1, driver, logger), "Clicking on Reschedule in Dropdown", driver, logger);
		Thread.sleep(6000);
		
		//Check-in to Preconsulting
		Web_GeneralFunctions.scrollElementToCenter(ASpage.getAppointmentDropDown(n, driver, logger), driver);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(n, driver, logger), "Clicking on Dropdown in Checked in  Appointment", driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(n,1, driver, logger), "Clicking on Reschedule in Dropdown", driver, logger);
		Thread.sleep(6000);
		
		//Preconsulting to Appointment Dashboard
		Web_GeneralFunctions.click(CPage.getPoweredByLink(driver, logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		Thread.sleep(5000);
		
		//Preconsulting to Preconsulted
		Web_GeneralFunctions.scrollElementToCenter(ASpage.getAppointmentDropDown(n, driver, logger), driver);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(n, driver, logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(n,1, driver, logger), "Clicking on Reschedule in Dropdown", driver, logger);
		Thread.sleep(6000);
		
		//preconsulted to All Slots Page
		Web_GeneralFunctions.scrollToElement(PreconPage.getSaveBtn(driver, logger), driver);
		Web_GeneralFunctions.click(PreconPage.getSaveBtn(driver, logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		Thread.sleep(6000);
		
		//Moving to All slots Page
		Thread.sleep(6000);
	}
	
	
	
	
	
	
	
	
	
	public synchronized void ScrollAndBookAllSlots(String slotId) throws Exception {
		
		
		
		AllSlotsPage ASpage = new AllSlotsPage();
		Thread.sleep(3000);
		Web_GeneralFunctions.scrollElementToCenter(ASpage.getPlusIconforAvailableSlot(slotId, driver, logger), driver);
		Thread.sleep(3000);
		Web_GeneralFunctions.click(ASpage.getPlusIconforAvailableSlot(slotId, driver, logger), "Clicking on Plus Icon", driver, logger);
		Web_GeneralFunctions.sendkeys(ASpage.getSearchBoxinAvailableSlot(slotId, driver, logger), "8553406065", "sending value in search box", driver, logger);
		Thread.sleep(3000);
		Web_GeneralFunctions.click(ASpage.getFirstAvailableConsumer(driver, logger), "Clicking on first available consumer", driver, logger);
		Web_GeneralFunctions.sendkeys(ASpage.setReasonforVisit(driver, logger), "EMR Automation Testing", "Setting Visit Reason", driver, logger);
		Web_GeneralFunctions.click(ASpage.getBookAppointmentSubmitBtn(driver, logger), "Clicking to Submit Appointment", driver, logger);
		Thread.sleep(6000);
		
	}
	
	
	
	public synchronized void appCountCheck(int prevCnt,boolean inc) throws Exception {
		
		AppointmentsPage appoiPage = new AppointmentsPage();
		int appCnt = appoiPage.getAppointmentCount(driver, logger);
		if(inc&&appCnt==prevCnt+1) {
			System.out.println("Count Properly Increased");
		}else if(!inc&&appCnt==prevCnt-1) {
			System.out.println("Count Properly Increased");
		}else {
			System.out.println("Error: Please Check Counts Again");
		}
		
		
	}
	
	
	
	public synchronized void CheinCountCheck(int prevCnt,boolean inc) throws Exception {
		
		AppointmentsPage appoiPage = new AppointmentsPage();
		int appCnt = appoiPage.getCheckedinCount(driver, logger);
		if(inc&&appCnt==prevCnt+1) {
			System.out.println("Count Properly Increased");
		}else if(!inc&&appCnt==prevCnt-1) {
			System.out.println("Count Properly Increased");
		}else {
			System.out.println("Error: Please Check Counts Again");
		}
		
		
	}
	
	
	public synchronized void CompletedCountCheck(int prevCnt,boolean inc) throws Exception {
		
		AppointmentsPage appoiPage = new AppointmentsPage();
		int appCnt = appoiPage.getCheckedinCount(driver, logger);
		if(inc&&appCnt==prevCnt+1) {
			System.out.println("Count Properly Increased");
		}else if(!inc&&appCnt==prevCnt-1) {
			System.out.println("Count Properly Increased");
		}else {
			System.out.println("Error: Please Check Counts Again");
		}
		
		
	}
	
	
	public synchronized void ConsultPopupCheck() throws Exception {
		
		
		CommonPage CPage = new CommonPage();
		try {
			if(CPage.getConsultationAlert(driver, logger,10).isDisplayed()) {
				Web_GeneralFunctions.click(CPage.getConsultationAlert(driver, logger), "Clicking to Checkout and start new Appointment", driver, logger);
			}
		}catch (Exception e) {
			
		}
		
	}
	
	
	public synchronized void NoDiagnosisCheck() throws Exception {
		
		
		ConsultationPage ConsPage = new ConsultationPage();
		
		if(ConsPage.getYesinSweetAlert(driver, logger).isDisplayed()) {
			Web_GeneralFunctions.click(ConsPage.getYesinSweetAlert(driver, logger),"Clicking to Checkout and start new Appointment", driver, logger);
		}
		
	}
	
	
	
			
}