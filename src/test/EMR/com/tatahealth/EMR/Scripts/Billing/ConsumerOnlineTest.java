package com.tatahealth.EMR.Scripts.Billing;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Consultation.ConsultationTest;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.Billing.Billing;
import com.tatahealth.EMR.pages.Consultation.ConsultationPage;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class ConsumerOnlineTest {

	ConsultationTest appt = new ConsultationTest();
	BillingTest bs = new BillingTest();
	Billing bill = new Billing();
	ConsultationPage consultation = new ConsultationPage();
	ConsumerAppointmentTest consumer = new ConsumerAppointmentTest();
	public static ExtentTest logger;

	@BeforeClass(alwaysRun = true)
	public static void beforeClass() throws Exception {
		Reports.reports();
		Login_Doctor.executionName = "ConsumerOnlineTest";
		 BillingTest.doctorName="Saket";
		Login_Doctor.LoginTestwithDiffrentUser(BillingTest.doctorName);
	}

	@AfterClass(alwaysRun = true)
	public static void afterClass() throws Exception {
		Login_Doctor.LogoutTest();
		Reports.extent.flush();
	}

	/* Billing_C_PP_2-pay at clinic is enabled from doctor side */
	/*
	 * Mention test case covered in all test below :RGB_5
	 * ,VB_1-VB_24,Biiling_C_P_27-Biiling_C_P_33,Biiling_C_P_18,Biiling_C_P_23-
	 * Biiling_C_P_26 Biiling_C_P_47-Biiling_C_P_48
	 */

	/*
	 * Test case:Billing_C_PP_16,Biiling_C_P_19-Biiling_C_P_21,Biiling_C_P_43_Online
	 */
	@Test(priority = 58, groups = { "G1" })
	public void consumerOPaymentCashWalletEmrValidation() throws Exception {
		logger = Reports.extent.createTest("consumer Online Payment Emr Validation with Cash Wallet");
		String paymentCheck = "Cash, Wallet, Online";
		String paidStatus = "Cash, Wallet";
		consumer.consumerBookAppointment("Online Payment");
		bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
		consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(5);
		appt.getAppointmentDropdown();
		appt.appointmentBookedStatus();
		appt.appointmentPaymentModeOStatusBooked();
		appt.bookedAppointmentOptionValidation();
		appt.getAppointmentDropDownValue(2, "Click on checkin and Generate Bill");
		consumer.consumerOBillingPageValidation();
		bs.paymentCashWallet();
		bs.billingCreateBillPopUpValidation();
		bs.billingCashWalletPdfValidation();
		Web_GeneralFunctions.wait(5);
		consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(5);
		appt.getAppointmentDropdown();
		appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		appt.appointmentPaymentModeOStatusPaid(paidStatus);
		consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		bs.paymentRemoveSelectedCashWalletOption();
		bs.billingCreatePaymentOptionDisplay();
		bs.paymentCashWallet();
		bs.billingCreateBillPopUpValidation();
		bs.billingCashWalletPdfValidation();

	}

	@Test(priority = 59, groups = { "G2" })
	public void consumerOPaymentCashDebitCardEmrValidation() throws Exception {
		logger = Reports.extent.createTest("consumer Online Payment Emr Validation with Cash Debit Card");
		String paymentCheck = "Cash, Debit card, Online";
		String paidStatus = "Cash, Debit card";
		consumer.consumerBookAppointment("Online Payment");
		bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
		consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(5);
		appt.getAppointmentDropdown();
		appt.appointmentBookedStatus();
		appt.appointmentPaymentModeOStatusBooked();
		appt.bookedAppointmentOptionValidation();
		appt.getAppointmentDropDownValue(2, "Click on checkin and Generate Bill");
		consumer.consumerOBillingPageValidation();
		bs.paymentCashDebitCard();
		bs.billingCreateBillPopUpValidation();
		bs.billingCashCardPdfValidation();
		Web_GeneralFunctions.wait(5);
		consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(5);
		appt.getAppointmentDropdown();
		appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		appt.appointmentPaymentModeOStatusPaid(paidStatus);
		consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		bs.paymentRemoveSelectedCashCardOption();
		bs.billingCreatePaymentOptionDisplay();
		bs.paymentCashDebitCard();
		bs.billingCreateBillPopUpValidation();
		bs.billingCashCardPdfValidation();

	}

	@Test(priority = 60, groups = { "G3" })
	public void consumerOPaymentCashCreditCardEmrValidation() throws Exception {
		logger = Reports.extent.createTest("consumer Online Payment Emr Validation with Cash Credit Card");
		String paymentCheck = "Cash, Credit card, Online";
		String paidStatus = "Cash, Credit card";
		consumer.consumerBookAppointment("Online Payment");
		bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
		consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(5);
		appt.getAppointmentDropdown();
		appt.appointmentBookedStatus();
		appt.appointmentPaymentModeOStatusBooked();
		appt.bookedAppointmentOptionValidation();
		appt.getAppointmentDropDownValue(2, "Click on checkin and Generate Bill");
		consumer.consumerOBillingPageValidation();
		bs.paymentCashCreditCard();
		bs.billingCreateBillPopUpValidation();
		bs.billingCashCardPdfValidation();
		Web_GeneralFunctions.wait(5);
		consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(5);
		appt.getAppointmentDropdown();
		appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		appt.appointmentPaymentModeOStatusPaid(paidStatus);
		consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		bs.paymentRemoveSelectedCashCardOption();
		bs.billingCreatePaymentOptionDisplay();
		bs.paymentCashCreditCard();
		bs.billingCreateBillPopUpValidation();
		bs.billingCashCardPdfValidation();

	}

	@Test(priority = 61, groups = { "G1" })
	public void consumerOPaymentCashEmrValidation() throws Exception {
		logger = Reports.extent.createTest("consumer Online Payment Emr Validation with Cash");
		String paymentCheck = "Cash, Online";
		String paidStatus = "Cash";
		consumer.consumerBookAppointment("Online Payment");
		bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
		consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(5);
		appt.getAppointmentDropdown();
		appt.appointmentBookedStatus();
		appt.appointmentPaymentModeOStatusBooked();
		appt.bookedAppointmentOptionValidation();
		appt.getAppointmentDropDownValue(2, "Click on checkin and Generate Bill");
		consumer.consumerOBillingPageValidation();
		bs.paymentCash();
		bs.billingCreateBillPopUpValidation();
		bs.billingCashPdfValidation();
		Web_GeneralFunctions.wait(5);
		consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(5);
		appt.getAppointmentDropdown();
		appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		appt.appointmentPaymentModeOStatusPaid(paidStatus);
		consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		bs.paymentRemoveSelectedCashOption();
		bs.billingCreatePaymentOptionDisplay();
		bs.paymentCash();
		bs.billingCreateBillPopUpValidation();
		bs.billingCashPdfValidation();

	}

	@Test(priority = 62, groups = { "G2" })
	public void consumerOPaymentCreditCardEmrValidation() throws Exception {
		logger = Reports.extent.createTest("consumer Online Payment Emr Validation with Credit Card");
		String paymentCheck = "Credit card, Online";
		String paidStatus = "Credit card";
		consumer.consumerBookAppointment("Online Payment");
		bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
		consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(5);
		appt.getAppointmentDropdown();
		appt.appointmentBookedStatus();
		appt.appointmentPaymentModeOStatusBooked();
		appt.bookedAppointmentOptionValidation();
		appt.getAppointmentDropDownValue(2, "Click on checkin and Generate Bill");
		consumer.consumerOBillingPageValidation();
		bs.paymentCreditCard();
		bs.billingCreateBillPopUpValidation();
		bs.billingCardPdfValidation();
		Web_GeneralFunctions.wait(5);
		consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(5);
		appt.getAppointmentDropdown();
		appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		appt.appointmentPaymentModeOStatusPaid(paidStatus);
		consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		bs.paymentRemoveSelectedCardOption();
		bs.billingCreatePaymentOptionDisplay();
		bs.paymentCreditCard();
		bs.billingCreateBillPopUpValidation();
		bs.billingCardPdfValidation();

	}

	@Test(priority = 63, groups = { "G3" })
	public void consumerOPaymentWalletEmrValidation() throws Exception {
		logger = Reports.extent.createTest("consumer Online Payment Emr Validation with Wallet");
		String paymentCheck = "Wallet, Online";
		String paidStatus = "Wallet";
		consumer.consumerBookAppointment("Online Payment");
		bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
		consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(5);
		appt.getAppointmentDropdown();
		appt.appointmentBookedStatus();
		appt.appointmentPaymentModeOStatusBooked();
		appt.bookedAppointmentOptionValidation();
		appt.getAppointmentDropDownValue(2, "Click on checkin and Generate Bill");
		consumer.consumerOBillingPageValidation();
		bs.paymentWallet();
		bs.billingCreateBillPopUpValidation();
		bs.billingWalletPdfValidation();
		Web_GeneralFunctions.wait(5);
		consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(5);
		appt.getAppointmentDropdown();
		appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		appt.appointmentPaymentModeOStatusPaid(paidStatus);
		consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		bs.paymentRemoveSelectedWalletOption();
		bs.billingCreatePaymentOptionDisplay();
		bs.paymentWallet();
		bs.billingCreateBillPopUpValidation();
		bs.billingWalletPdfValidation();

	}

}
