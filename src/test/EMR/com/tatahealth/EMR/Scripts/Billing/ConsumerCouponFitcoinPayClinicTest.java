package com.tatahealth.EMR.Scripts.Billing;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Consultation.ConsultationTest;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.Billing.Billing;
import com.tatahealth.EMR.pages.Consultation.ConsultationPage;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class ConsumerCouponFitcoinPayClinicTest {
	
	ConsultationTest appt=new ConsultationTest();
	BillingTest bs=new BillingTest();
	Billing bill = new Billing();
	ConsultationPage consultation = new ConsultationPage();
	ConsumerAppointmentTest consumer= new ConsumerAppointmentTest();
	public static ExtentTest logger;
	
	
	@BeforeClass(alwaysRun = true) public static void beforeClass() throws Exception { 
		  Reports.reports();
		  Login_Doctor.executionName="ConsumerCouponFitcoinPayClinicTest";
		  BillingTest.doctorName="Baba";
		  Login_Doctor.LoginTestwithDiffrentUser(BillingTest.doctorName);
		  }
	  
	  @AfterClass(alwaysRun = true) public static void afterClass() throws Exception {
		  Login_Doctor.LogoutTest();
			Reports.extent.flush(); 
		  }
	  
	  
	  /*Billing_C_PP_2-pay at clinic is enabled from doctor side*/
		/*  Mention test case covered in all test below :RGB_5 ,VB_1-VB_24,Biiling_C_P_27-Biiling_C_P_33,Biiling_C_P_18,Biiling_C_P_23-Biiling_C_P_26
		 * Biiling_C_P_47-Biiling_C_P_48
		 */
	  
		
		/*Test case:Billing_C_PP_6,Biiling_C_P_3,Biiling_C_P_5*/
		@Test(priority=151,groups = {"G2"})
		public void consumerCFPcPaymentCashWalletEmrValidation()throws Exception
		{
			logger = Reports.extent.createTest("consumer Coupon+Fitcoin+Pay at Clinic Payment Emr Validation with Cash Wallet");
			String paymentCheck="Cash, Wallet, Rewards, Coupon";
			String paidStatus="Cash, Wallet";
			consumer.consumerBookAppointment("Coupon + Fit Coin + Pay At Clinic");
			bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.appointmentPaymentModeCFPcStatusBooked();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
			 consumer.consumerCFPcBillingPageValidation();
			 bs.paymentCashWallet();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingCashWalletPdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.appointmentPaymentModeCFPcStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		  bs.paymentRemoveSelectedCashWalletOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentCashWallet();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingCashWalletPdfValidation();
		 
		}
		@Test(priority=152,groups = {"G1"})
		public void consumerCFPcPaymentCashDebitCardEmrValidation()throws Exception
		{
			logger = Reports.extent.createTest("consumer Coupon+Fitcoin+Pay at Clinic Payment Emr Validation with Cash Debit Card");
			String paymentCheck="Cash, Debit card, Rewards, Coupon";
			String paidStatus="Cash, Debit card";
			consumer.consumerBookAppointment("Coupon + Fit Coin + Pay At Clinic");
			bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.appointmentPaymentModeCFPcStatusBooked();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
			 consumer.consumerCFPcBillingPageValidation();
			 bs.paymentCashDebitCard();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingCashCardPdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.appointmentPaymentModeCFPcStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		  bs.paymentRemoveSelectedCashCardOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentCashDebitCard();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingCashCardPdfValidation();
		 
		}
		@Test(priority=153,groups = {"G3"})
		public void consumerCFPcPaymentCashCreditCardEmrValidation()throws Exception
		{
			logger = Reports.extent.createTest("consumer Coupon+Fitcoin+Pay at Clinic Payment Emr Validation with Cash Credit Card");
			String paymentCheck="Cash, Credit card, Rewards, Coupon";
			String paidStatus="Cash, Credit card";
			consumer.consumerBookAppointment("Coupon + Fit Coin + Pay At Clinic");
			bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.appointmentPaymentModeCFPcStatusBooked();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
			 consumer.consumerCFPcBillingPageValidation();
			 bs.paymentCashCreditCard();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingCashCardPdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.appointmentPaymentModeCFPcStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		  bs.paymentRemoveSelectedCashCardOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentCashCreditCard();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingCashCardPdfValidation();
		 
		}
		
		@Test(priority=154,groups = {"G1"})
		public void consumerCFPcPaymentCashEmrValidation()throws Exception
		{
			logger = Reports.extent.createTest("consumer Coupon+Fitcoin+Pay at Clinic Payment Emr Validation with Cash");
			String paymentCheck="Cash, Rewards, Coupon";
			String paidStatus="Cash";
			consumer.consumerBookAppointment("Coupon + Fit Coin + Pay At Clinic");
			bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.appointmentPaymentModeCFPcStatusBooked();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
			 consumer.consumerCFPcBillingPageValidation();
			 bs.paymentCash();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingCashPdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.appointmentPaymentModeCFPcStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		  bs.paymentRemoveSelectedCashOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentCash();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingCashPdfValidation();
		 
		}
		@Test(priority=155,groups = {"G2"})
		public void consumerCFPcPaymentCreditCardEmrValidation()throws Exception
		{
			logger = Reports.extent.createTest("consumer Coupon+Fitcoin+Pay at Clinic Payment Emr Validation with Credit Card");
			String paymentCheck="Credit card, Rewards, Coupon";
			String paidStatus="Credit card";
			consumer.consumerBookAppointment("Coupon + Fit Coin + Pay At Clinic");
			bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.appointmentPaymentModeCFPcStatusBooked();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
			 consumer.consumerCFPcBillingPageValidation();
			 bs.paymentCreditCard();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingCardPdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.appointmentPaymentModeCFPcStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		  bs.paymentRemoveSelectedCardOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentCreditCard();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingCardPdfValidation();
		 
		}
		@Test(priority=156,groups = {"G3"})
		public void consumerCFPcPaymentWalletEmrValidation()throws Exception
		{
			logger = Reports.extent.createTest("consumer Coupon+Fitcoin+Pay at Clinic Payment Emr Validation with Wallet");
			String paymentCheck="Wallet, Rewards, Coupon";
			String paidStatus="Wallet";
			consumer.consumerBookAppointment("Coupon + Fit Coin + Pay At Clinic");
			bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.appointmentPaymentModeCFPcStatusBooked();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
			 consumer.consumerCFPcBillingPageValidation();
			 bs.paymentWallet();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingWalletPdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.appointmentPaymentModeCFPcStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		  bs.paymentRemoveSelectedWalletOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentWallet();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingWalletPdfValidation();
		 
		}
		
		//With No Service
		
		@Test(priority=157,groups = {"G1"})
		public void consumerCFPcPaymentCashWalletNoServiceEmrValidation()throws Exception
		{
			logger = Reports.extent.createTest("consumer Coupon+Fitcoin+Pay at Clinic Payment Emr Validation with Cash Wallet No Service");
			String paymentCheck="Cash, Wallet, Rewards, Coupon";
			String paidStatus="Cash, Wallet";
			consumer.consumerBookAppointment("Coupon + Fit Coin + Pay At Clinic");
			bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.appointmentPaymentModeCFPcStatusBooked();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
			 consumer.consumerCFPcBillingPageNoServiceValidation();
			 bs.paymentCashWallet();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingCashWalletNoServicePdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.appointmentPaymentModeCFPcStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		  bs.paymentRemoveSelectedCashWalletOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentCashWallet();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingCashWalletNoServicePdfValidation();
		 
		}
		@Test(priority=158,groups = {"G2"})
		public void consumerCFPcPaymentCashDebitCardNoServiceEmrValidation()throws Exception
		{
			logger = Reports.extent.createTest("consumer Coupon+Fitcoin+Pay at Clinic Payment Emr Validation with Cash Debit Card No Service");
			String paymentCheck="Cash, Debit card, Rewards, Coupon";
			String paidStatus="Cash, Debit card";
			consumer.consumerBookAppointment("Coupon + Fit Coin + Pay At Clinic");
			bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.appointmentPaymentModeCFPcStatusBooked();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
			 consumer.consumerCFPcBillingPageNoServiceValidation();
			 bs.paymentCashDebitCard();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingCashCardNoServicePdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.appointmentPaymentModeCFPcStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		  bs.paymentRemoveSelectedCashCardOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentCashDebitCard();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingCashCardNoServicePdfValidation();
		 
		}
		
		@Test(priority=159,groups = {"G2"})
		public void consumerCFPcPaymentCashNoServiceEmrValidation()throws Exception
		{
			logger = Reports.extent.createTest("consumer Coupon+Fitcoin+Pay at Clinic Payment Emr Validation with Cash No Service");
			String paymentCheck="Cash, Rewards, Coupon";
			String paidStatus="Cash";
			consumer.consumerBookAppointment("Coupon + Fit Coin + Pay At Clinic");
			bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.appointmentPaymentModeCFPcStatusBooked();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
			 consumer.consumerCFPcBillingPageNoServiceValidation();
			 bs.paymentCash();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingCashNoServicePdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.appointmentPaymentModeCFPcStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		  bs.paymentRemoveSelectedCashOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentCash();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingCashNoServicePdfValidation();
		 
		}
		@Test(priority=160,groups = {"G1"})
		public void consumerCFPcPaymentCreditCardNoServiceEmrValidation()throws Exception
		{
			logger = Reports.extent.createTest("consumer Coupon+Fitcoin+Pay at Clinic Payment Emr Validation with Credit Card No Service");
			String paymentCheck="Credit card, Rewards, Coupon";
			String paidStatus="Credit card";
			consumer.consumerBookAppointment("Coupon + Fit Coin + Pay At Clinic");
			bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.appointmentPaymentModeCFPcStatusBooked();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
			 consumer.consumerCFPcBillingPageNoServiceValidation();
			 bs.paymentCreditCard();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingCardNoServicePdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.appointmentPaymentModeCFPcStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		  bs.paymentRemoveSelectedCardOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentCreditCard();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingCardNoServicePdfValidation();
		 
		}
		@Test(priority=161,groups = {"G3"})
		public void consumerCFPcPaymentWalletNoServiceEmrValidation()throws Exception
		{
			logger = Reports.extent.createTest("consumer Coupon+Fitcoin+Pay at Clinic Payment Emr Validation with Wallet No Service");
			String paymentCheck="Wallet, Rewards, Coupon";
			String paidStatus="Wallet";
			consumer.consumerBookAppointment("Coupon + Fit Coin + Pay At Clinic");
			bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.appointmentPaymentModeCFPcStatusBooked();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
			 consumer.consumerCFPcBillingPageNoServiceValidation();
			 bs.paymentWallet();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingWalletNoServicePdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.appointmentPaymentModeCFPcStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		  bs.paymentRemoveSelectedWalletOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentWallet();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingWalletNoServicePdfValidation();
		 
		}

}
