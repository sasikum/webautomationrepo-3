package com.tatahealth.EMR.Scripts.Billing;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Consultation.ConsultationTest;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.Billing.Billing;
import com.tatahealth.EMR.pages.Consultation.ConsultationPage;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class ConsumerCouponFitcoinOnlineTest {
		
		ConsultationTest appt=new ConsultationTest();
		BillingTest bs=new BillingTest();
		Billing bill = new Billing();
		ConsultationPage consultation = new ConsultationPage();
		ConsumerAppointmentTest consumer= new ConsumerAppointmentTest();
		public static ExtentTest logger;
		
		
		@BeforeClass(alwaysRun = true) 
		public static void beforeClass() throws Exception { 
			  Reports.reports();
			  Login_Doctor.executionName="ConsumerCouponFitcoinOnlineTest";
			  BillingTest.doctorName="Don";
			  Login_Doctor.LoginTestwithDiffrentUser(BillingTest.doctorName);
			  }
		  
		  @AfterClass(alwaysRun = true)
		  public static void afterClass() throws Exception {
			  Login_Doctor.LogoutTest();
				Reports.extent.flush(); 
			  }
		  
		  
		  /*Billing_C_PP_2-pay at clinic is enabled from doctor side*/
			/*  Mention test case covered in all test below :RGB_5 ,VB_1-VB_24,Biiling_C_P_27-Biiling_C_P_33,Biiling_C_P_18,Biiling_C_P_23-Biiling_C_P_26
			 * Biiling_C_P_47-Biiling_C_P_48
			 */
		  /*Test case:Billing_C_PP_7,Biiling_C_P_3,Biiling_C_P_5*/
			@Test(priority=162,groups = {"G1"})
			public void consumerCFOPaymentCashWalletEmrValidation()throws Exception
			{
				logger = Reports.extent.createTest("consumer Coupon+Fitcoin+Online Payment Emr Validation with Cash Wallet");
				String paymentCheck="Cash, Wallet, Online, Rewards, Coupon";
				String paidStatus="Cash, Wallet";
				consumer.consumerBookAppointment("Coupon + Fit Coin + Online Payment");
				bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
				Web_GeneralFunctions.wait(2);
				 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
				 Web_GeneralFunctions.wait(5);
				appt.getAppointmentDropdown();
				 appt.appointmentBookedStatus();
				 appt.appointmentPaymentModeCFOStatusBooked();
				 appt.bookedAppointmentOptionValidation();
				 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
				 consumer.consumerCFOBillingPageValidation();
				 bs.paymentCashWallet();
				 bs.billingCreateBillPopUpValidation();
				 bs.billingCashWalletPdfValidation();
				 Web_GeneralFunctions.wait(5);
			  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			  Web_GeneralFunctions.wait(5);
			  appt.getAppointmentDropdown();
			  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
			  appt.appointmentPaymentModeCFOStatusPaid(paidStatus);
			  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
			  bs.paymentRemoveSelectedCashWalletOption();
			  bs. billingCreateBillPaymentValidation();
			  bs.paymentCashWallet();
			  bs. billingCreateBillPopUpValidation();
			  bs.billingCashWalletPdfValidation();
			 
			}
			@Test(priority=163,groups = {"G2"})
			public void consumerCFOPaymentCashDebitCardEmrValidation()throws Exception
			{
				logger = Reports.extent.createTest("consumer Coupon+Fitcoin+Online Payment Emr Validation with Cash Debit Card");
				String paymentCheck="Cash, Debit card, Online, Rewards, Coupon";
				String paidStatus="Cash, Debit card";
				consumer.consumerBookAppointment("Coupon + Fit Coin + Online Payment");
				bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
				Web_GeneralFunctions.wait(2);
				 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
				 Web_GeneralFunctions.wait(5);
				appt.getAppointmentDropdown();
				 appt.appointmentBookedStatus();
				 appt.appointmentPaymentModeCFOStatusBooked();
				 appt.bookedAppointmentOptionValidation();
				 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
				 consumer.consumerCFOBillingPageValidation();
				 bs.paymentCashDebitCard();
				 bs.billingCreateBillPopUpValidation();
				 bs.billingCashCardPdfValidation();
				 Web_GeneralFunctions.wait(5);
			  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			  Web_GeneralFunctions.wait(5);
			  appt.getAppointmentDropdown();
			  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
			  appt.appointmentPaymentModeCFOStatusPaid(paidStatus);
			  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
			  bs.paymentRemoveSelectedCashCardOption();
			  bs.billingCreatePaymentOptionDisplay();
			  bs.paymentCashDebitCard();
			  bs. billingCreateBillPopUpValidation();
			  bs.billingCashCardPdfValidation();
			 
			}
			@Test(priority=164,groups = {"G3"})
			public void consumerCFOPaymentCashCreditCardEmrValidation()throws Exception
			{
				logger = Reports.extent.createTest("consumer Coupon+Fitcoin+Online Payment Emr Validation with Cash Creditt Card");
				String paymentCheck="Cash, Credit card, Online, Rewards, Coupon";
				String paidStatus="Cash, Credit card";
				consumer.consumerBookAppointment("Coupon + Fit Coin + Online Payment");
				bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
				Web_GeneralFunctions.wait(2);
				 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
				 Web_GeneralFunctions.wait(5);
				appt.getAppointmentDropdown();
				 appt.appointmentBookedStatus();
				 appt.appointmentPaymentModeCFOStatusBooked();
				 appt.bookedAppointmentOptionValidation();
				 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
				 consumer.consumerCFOBillingPageValidation();
				 bs.paymentCashCreditCard();
				 bs.billingCreateBillPopUpValidation();
				 bs.billingCashCardPdfValidation();
				 Web_GeneralFunctions.wait(5);
			  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			  Web_GeneralFunctions.wait(5);
			  appt.getAppointmentDropdown();
			  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
			  appt.appointmentPaymentModeCFOStatusPaid(paidStatus);
			  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
			  bs.paymentRemoveSelectedCashCardOption();
			  bs.billingCreatePaymentOptionDisplay();
			  bs.paymentCashCreditCard();
			  bs. billingCreateBillPopUpValidation();
			  bs.billingCashCardPdfValidation();
			 
			}
			@Test(priority=165,groups = {"G2"})
			public void consumerCFOPaymentCashEmrValidation()throws Exception
			{
				logger = Reports.extent.createTest("consumer Coupon+Fitcoin+Online Payment Emr Validation with Cash ");
				String paymentCheck="Cash, Online, Rewards, Coupon";
				String paidStatus="Cash";
				consumer.consumerBookAppointment("Coupon + Fit Coin + Online Payment");
				bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
				Web_GeneralFunctions.wait(2);
				 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
				 Web_GeneralFunctions.wait(5);
				appt.getAppointmentDropdown();
				 appt.appointmentBookedStatus();
				 appt.appointmentPaymentModeCFOStatusBooked();
				 appt.bookedAppointmentOptionValidation();
				 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
				 consumer.consumerCFOBillingPageValidation();
				 bs.paymentCash();
				 bs.billingCreateBillPopUpValidation();
				 bs.billingCashPdfValidation();
				 Web_GeneralFunctions.wait(5);
			  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			  Web_GeneralFunctions.wait(5);
			  appt.getAppointmentDropdown();
			  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
			  appt.appointmentPaymentModeCFOStatusPaid(paidStatus);
			  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
			  bs.paymentRemoveSelectedCashOption();
			  bs. billingCreateBillPaymentValidation();
			  bs.paymentCash();
			  bs. billingCreateBillPopUpValidation();
			  bs.billingCashPdfValidation();
			 
			}
			@Test(priority=166,groups = {"G1"})
			public void consumerCFOPaymentCreditCardEmrValidation()throws Exception
			{
				logger = Reports.extent.createTest("consumer Coupon+Fitcoin+Online Payment Emr Validation with Credit card");
				String paymentCheck="Credit card, Online, Rewards, Coupon";
				String paidStatus="Credit card";
				consumer.consumerBookAppointment("Coupon + Fit Coin + Online Payment");
				bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
				Web_GeneralFunctions.wait(2);
				 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
				 Web_GeneralFunctions.wait(5);
				appt.getAppointmentDropdown();
				 appt.appointmentBookedStatus();
				 appt.appointmentPaymentModeCFOStatusBooked();
				 appt.bookedAppointmentOptionValidation();
				 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
				 consumer.consumerCFOBillingPageValidation();
				 bs.paymentCreditCard();
				 bs.billingCreateBillPopUpValidation();
				 bs.billingCardPdfValidation();
				 Web_GeneralFunctions.wait(5);
			  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			  Web_GeneralFunctions.wait(5);
			  appt.getAppointmentDropdown();
			  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
			  appt.appointmentPaymentModeCFOStatusPaid(paidStatus);
			  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
			  bs.paymentRemoveSelectedCardOption();
			  bs. billingCreateBillPaymentValidation();
			  bs.paymentCreditCard();
			  bs. billingCreateBillPopUpValidation();
			  bs.billingCardPdfValidation();
			 
			}
			@Test(priority=167,groups = {"G3"})
			public void consumerCFOPaymentWalletEmrValidation()throws Exception
			{
				logger = Reports.extent.createTest("consumer Coupon+Fitcoin+Online Payment Emr Validation with Wallet");
				String paymentCheck="Wallet, Online, Rewards, Coupon";
				String paidStatus="Wallet";
				consumer.consumerBookAppointment("Coupon + Fit Coin + Online Payment");
				bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
				Web_GeneralFunctions.wait(2);
				 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
				 Web_GeneralFunctions.wait(5);
				appt.getAppointmentDropdown();
				 appt.appointmentBookedStatus();
				 appt.appointmentPaymentModeCFOStatusBooked();
				 appt.bookedAppointmentOptionValidation();
				 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
				 consumer.consumerCFOBillingPageValidation();
				 bs.paymentWallet();
				 bs.billingCreateBillPopUpValidation();
				 bs.billingWalletPdfValidation();
				 Web_GeneralFunctions.wait(5);
			  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			  Web_GeneralFunctions.wait(5);
			  appt.getAppointmentDropdown();
			  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
			  appt.appointmentPaymentModeCFOStatusPaid(paidStatus);
			  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
			  bs.paymentRemoveSelectedWalletOption();
			  bs. billingCreateBillPaymentValidation();
			  bs.paymentWallet();
			  bs. billingCreateBillPopUpValidation();
			  bs.billingWalletPdfValidation();
			 
			}
		  
}
