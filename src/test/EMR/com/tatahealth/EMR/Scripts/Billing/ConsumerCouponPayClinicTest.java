package com.tatahealth.EMR.Scripts.Billing;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Consultation.ConsultationTest;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.Billing.Billing;
import com.tatahealth.EMR.pages.Consultation.ConsultationPage;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class ConsumerCouponPayClinicTest {
	
	ConsultationTest appt=new ConsultationTest();
	BillingTest bs=new BillingTest();
	Billing bill = new Billing();
	ConsultationPage consultation = new ConsultationPage();
	ConsumerAppointmentTest consumer= new ConsumerAppointmentTest();
	public static ExtentTest logger;
	
	
	@BeforeClass(alwaysRun = true) public static void beforeClass() throws Exception { 
		  Reports.reports();
		  Login_Doctor.executionName="ConsumerCouponPayClinicTest";
		  BillingTest.doctorName="Don";
		  Login_Doctor.LoginTestwithDiffrentUser(BillingTest.doctorName);
		  }
	  
	  @AfterClass(alwaysRun = true) public static void afterClass() throws Exception {
		  Login_Doctor.LogoutTest();
			Reports.extent.flush(); 
		  }
	
	/*Billing_C_PP_2-pay at clinic is enabled from doctor side*/
	/*  Mention test case covered in all test below :RGB_5 ,VB_1-VB_24,Biiling_C_P_27-Biiling_C_P_33,Biiling_C_P_18,Biiling_C_P_23-Biiling_C_P_26
	 * Biiling_C_P_47-Biiling_C_P_48
	 */
	  
	  /*Test case:Billing_C_PP_3,Biiling_C_P_3*/
		@Test(priority=127,groups = {"G1"})
		public void consumerCPcPaymentCashWalletEmrValidation()throws Exception
		{
			logger = Reports.extent.createTest("consumer Coupon+Pay at Clinic Payment Emr Validation with Cash Wallet");
			String paymentCheck="Cash, Wallet, Coupon";
			String paidStatus="Cash, Wallet";
			consumer.consumerBookAppointment("Coupon + Pay At Clinic");
			bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.appointmentPaymentModeCPcStatusBooked();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
			 consumer.consumerCPcBillingPageValidation();
			 bs.paymentCashWallet();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingCashWalletPdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.appointmentPaymentModeCPcStatusPaid(paidStatus);
		  consumer. consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		  bs.paymentRemoveSelectedCashWalletOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentCashWallet();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingCashWalletPdfValidation();
		 
		}
		@Test(priority=128,groups = {"G2"})
		public void consumerCPcPaymentCashDebitCardEmrValidation()throws Exception
		{
			logger = Reports.extent.createTest("consumer Coupon+Pay at Clinic Payment Emr Validation with Cash Debit Card");
			String paymentCheck="Cash, Debit card, Coupon";
			String paidStatus="Cash, Debit card";
			consumer.consumerBookAppointment("Coupon + Pay At Clinic");
			bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.appointmentPaymentModeCPcStatusBooked();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
			 consumer.consumerCPcBillingPageValidation();
			 bs.paymentCashDebitCard();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingCashCardPdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.appointmentPaymentModeCPcStatusPaid(paidStatus);
		  consumer. consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		  bs.paymentRemoveSelectedCashCardOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentCashDebitCard();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingCashWalletPdfValidation();
		 
		}
		@Test(priority=129,groups = {"G3"})
		public void consumerCPcPaymentCashCreditCardEmrValidation()throws Exception
		{
			logger = Reports.extent.createTest("consumer Coupon+Pay at Clinic Payment Emr Validation with Cash Credit Card");
			String paymentCheck="Cash, Credit card, Coupon";
			String paidStatus="Cash, Credit card";
			consumer.consumerBookAppointment("Coupon + Pay At Clinic");
			bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.appointmentPaymentModeCPcStatusBooked();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
			 consumer.consumerCPcBillingPageValidation();
			 bs.paymentCashCreditCard();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingCashCardPdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.appointmentPaymentModeCPcStatusPaid(paidStatus);
		  consumer. consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		  bs.paymentRemoveSelectedCashCardOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentCashCreditCard();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingCashCardPdfValidation();
		 
		}
		@Test(priority=130,groups = {"G1"})
		public void consumerCPcPaymentCashEmrValidation()throws Exception
		{
			logger = Reports.extent.createTest("consumer Coupon+Pay at Clinic Payment Emr Validation with Cash");
			String paymentCheck="Cash, Coupon";
			String paidStatus="Cash";
			consumer.consumerBookAppointment("Coupon + Pay At Clinic");
			bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.appointmentPaymentModeCPcStatusBooked();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
			 consumer.consumerCPcBillingPageValidation();
			 bs.paymentCash();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingCashPdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.appointmentPaymentModeCPcStatusPaid(paidStatus);
		  consumer. consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		  bs.paymentRemoveSelectedCashOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentCash();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingCashPdfValidation();
		 
		}
		@Test(priority=131,groups = {"G3"})
		public void consumerCPcPaymentCreditCardEmrValidation()throws Exception
		{
			logger = Reports.extent.createTest("consumer Coupon+Pay at Clinic Payment Emr Validation with Credit Card");
			String paymentCheck="Credit card, Coupon";
			String paidStatus="Credit card";
			consumer.consumerBookAppointment("Coupon + Pay At Clinic");
			bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.appointmentPaymentModeCPcStatusBooked();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
			 consumer.consumerCPcBillingPageValidation();
			 bs.paymentCreditCard();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingCardPdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.appointmentPaymentModeCPcStatusPaid(paidStatus);
		  consumer. consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		  bs.paymentRemoveSelectedCardOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentCreditCard();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingCardPdfValidation();
		 
		}
		@Test(priority=132,groups = {"G2"})
		public void consumerCPcPaymentWalletEmrValidation()throws Exception
		{
			logger = Reports.extent.createTest("consumer Coupon+Pay at Clinic Payment Emr Validation with Wallet");
			String paymentCheck="Wallet, Coupon";
			String paidStatus="Wallet";
			consumer.consumerBookAppointment("Coupon + Pay At Clinic");
			bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.appointmentPaymentModeCPcStatusBooked();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
			 consumer.consumerCPcBillingPageValidation();
			 bs.paymentWallet();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingWalletPdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.appointmentPaymentModeCPcStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		  bs.paymentRemoveSelectedWalletOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentWallet();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingWalletPdfValidation();
		 
		}
		
		//No service 
		@Test(priority=133,groups = {"G1"})
		public void consumerCPcPaymentCashWalletNoServiceEmrValidation()throws Exception
		{
			logger = Reports.extent.createTest("consumer Coupon+Pay at Clinic Payment Emr Validation with Cash Wallet No Service");
			String paymentCheck="Cash, Wallet, Coupon";
			String paidStatus="Cash, Wallet";
			consumer.consumerBookAppointment("Coupon + Pay At Clinic");
			bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.appointmentPaymentModeCPcStatusBooked();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
			 consumer.consumerCPcBillingPageNoServiceValidation();
			 bs.paymentCashWallet();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingCashWalletNoServicePdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.appointmentPaymentModeCPcStatusPaid(paidStatus);
		  consumer. consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		  bs.paymentRemoveSelectedCashWalletOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentCashWallet();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingCashWalletNoServicePdfValidation();
		 
		}
		@Test(priority=134,groups = {"G3"})
		public void consumerCPcPaymentCashDebitCardNoServiceEmrValidation()throws Exception
		{
			logger = Reports.extent.createTest("consumer Coupon+Pay at Clinic Payment Emr Validation with Cash Debit Card No Service");
			String paymentCheck="Cash, Debit card, Coupon";
			String paidStatus="Cash, Debit card";
			consumer.consumerBookAppointment("Coupon + Pay At Clinic");
			bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.appointmentPaymentModeCPcStatusBooked();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
			 consumer.consumerCPcBillingPageNoServiceValidation();
			 bs.paymentCashDebitCard();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingCashCardNoServicePdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.appointmentPaymentModeCPcStatusPaid(paidStatus);
		  consumer. consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		  bs.paymentRemoveSelectedCashCardOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentCashDebitCard();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingCashWalletNoServicePdfValidation();
		 
		}
		@Test(priority=135,groups = {"G2"})
		public void consumerCPcPaymentCashNoServiceEmrValidation()throws Exception
		{
			logger = Reports.extent.createTest("consumer Coupon+Pay at Clinic Payment Emr Validation with Cash No Service");
			String paymentCheck="Cash, Coupon";
			String paidStatus="Cash";
			consumer.consumerBookAppointment("Coupon + Pay At Clinic");
			bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.appointmentPaymentModeCPcStatusBooked();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
			 consumer.consumerCPcBillingPageNoServiceValidation();
			 bs.paymentCash();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingCashNoServicePdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.appointmentPaymentModeCPcStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		  bs.paymentRemoveSelectedCashOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentCash();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingCashNoServicePdfValidation();
		 
		}
		@Test(priority=136,groups = {"G2"})
		public void consumerCPcPaymentCreditCardNoServiceEmrValidation()throws Exception
		{
			logger = Reports.extent.createTest("consumer Coupon+Pay at Clinic Payment Emr Validation with Credit Card No Service");
			String paymentCheck="Credit card, Coupon";
			String paidStatus="Credit card";
			consumer.consumerBookAppointment("Coupon + Pay At Clinic");
			bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.appointmentPaymentModeCPcStatusBooked();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
			 consumer.consumerCPcBillingPageNoServiceValidation();
			 bs.paymentCreditCard();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingCardNoServicePdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.appointmentPaymentModeCPcStatusPaid(paidStatus);
		  consumer. consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		  bs.paymentRemoveSelectedCardOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentCreditCard();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingCardNoServicePdfValidation();
		 
		}
		@Test(priority=137,groups = {"G1"})
		public void consumerCPcPaymentWalletNoServiceEmrValidation()throws Exception
		{
			logger = Reports.extent.createTest("consumer Coupon+Pay at Clinic Payment Emr Validation with Wallet No Service");
			String paymentCheck="Wallet, Coupon";
			String paidStatus="Wallet";
			consumer.consumerBookAppointment("Coupon + Pay At Clinic");
			bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.appointmentPaymentModeCPcStatusBooked();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
			 consumer.consumerCPcBillingPageNoServiceValidation();
			 bs.paymentWallet();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingWalletNoServicePdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.appointmentPaymentModeCPcStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		  bs.paymentRemoveSelectedWalletOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentWallet();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingWalletNoServicePdfValidation();
		 
		}

}
